-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 21, 2022 at 10:57 AM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sinamics`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `user_email`, `user_question`, `asked_at`, `eventname`, `speaker`, `answered`) VALUES
(1, 'viraj@coact.co.in', 'Test Que 01', '2021-03-09 12:43:55', 'ajantaapdrops2811', 0, 0),
(2, 'viraj@coact.co.in', 'Test Que. 02', '2021-03-09 12:44:04', 'ajantaapdrops2811', 0, 0),
(3, 'hadis.farhan@foxindia.net', 'The address is not available\r\n', '2021-03-09 14:12:46', 'ajantaapdrops2811', 0, 0),
(4, 'nikhil.thakre@ril.com', 'IS any app required to installed in mobile for commissioning ?\r\n', '2021-03-09 14:50:52', 'ajantaapdrops2811', 1, 0),
(5, 'nikhil.thakre@ril.com', 'For motor mounted G115, What will be the maximum \r\nAllowed vibration level ?', '2021-03-09 15:19:17', 'ajantaapdrops2811', 1, 0),
(6, 'vijayshankar.jadhav@siemens.com', 'Is G115D selection available in Sizer tool?', '2021-03-09 15:27:35', 'ajantaapdrops2811', 1, 0),
(7, 'vijayshankar.jadhav@siemens.com', 'Which types of encoders are permitted to be connected to G115D?', '2021-03-09 15:43:08', 'ajantaapdrops2811', 1, 0),
(8, 'alankar.manjrekar@siemens.com', 'Motor cable lengths: max. 15 m with shielded cable.\r\nCan we use any output side components for cable lenth more than 15 meter ?\r\n', '2021-03-09 16:05:30', 'ajantaapdrops2811', 0, 0),
(9, 'alankar.manjrekar@siemens.com', 'Motor cable lengths: max. 15 m with shielded cable.\r\nCan we use any output side components for cable lenth more than 15 meter ?\r\n', '2021-03-09 16:05:30', 'ajantaapdrops2811', 0, 0),
(10, 'arc@cnautomation.in', 'Up to what rating G115 D is available?\r\n', '2021-03-09 16:07:57', 'ajantaapdrops2811', 1, 0),
(11, 'kushaba.kokare@dacpl.in', 'please share video', '2021-03-09 16:09:36', 'ajantaapdrops2811', 0, 0),
(12, 'naresh.g@cotmac.io', 'Display not coming.\r\nOnly audio \r\n', '2021-03-11 14:12:14', 'ajantaapdrops2811', 0, 0),
(13, 'joy.aloor@foxindia.net', 'Looks very promising. When do deliveries begin? When do we get the pricing? ', '2021-03-11 15:00:22', 'ajantaapdrops2811', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_email`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(1, 'pandurevanthreddy002@gmail.com', NULL, NULL, '2021-02-21 22:09:39', '2021-02-21 22:09:39', '2021-02-21 22:10:09', 0, 'ajantaapdrops2811'),
(2, 'naganeerajkurra@gmail.com', NULL, NULL, '2021-02-21 22:19:00', '2021-03-11 15:06:30', '2021-03-11 15:08:01', 0, 'ajantaapdrops2811'),
(3, 'naganeerajreddy9989@gmail.com', NULL, NULL, '2021-02-21 22:20:09', '2021-03-11 15:07:03', '2021-03-11 15:07:33', 0, 'ajantaapdrops2811'),
(4, 'reynold@coact.co.in', NULL, NULL, '2021-02-22 10:04:06', '2021-03-09 14:16:26', '2021-03-09 14:16:56', 0, 'ajantaapdrops2811'),
(5, 'neeraj@coact.co.in', NULL, NULL, '2021-02-22 10:09:35', '2021-06-17 16:24:42', '2021-06-17 16:25:12', 1, 'ajantaapdrops2811'),
(6, 'viraj@coact.co.in', NULL, NULL, '2021-02-22 12:53:06', '2021-03-11 13:08:40', '2021-03-11 16:12:56', 0, 'ajantaapdrops2811'),
(7, 'pooja@coact.co.in', NULL, NULL, '2021-02-22 14:09:08', '2021-03-09 15:40:47', '2021-03-09 20:36:30', 0, 'ajantaapdrops2811'),
(8, 'nishanth@coact.co.in', NULL, NULL, '2021-02-23 10:26:52', '2021-03-09 15:19:23', '2021-03-09 15:19:53', 0, 'ajantaapdrops2811'),
(9, 'aj@gmail.com', NULL, NULL, '2021-02-23 10:27:29', '2021-02-23 10:27:29', '2021-02-23 10:28:29', 0, 'ajantaapdrops2811'),
(10, 'akshat@coact.co.in', NULL, NULL, '2021-02-23 10:50:12', '2021-02-23 10:50:12', '2021-02-23 10:51:12', 0, 'ajantaapdrops2811'),
(11, 'inthu6156@gmail.com', NULL, NULL, '2021-02-23 10:53:50', '2021-02-23 10:54:36', '2021-02-23 10:56:27', 0, 'ajantaapdrops2811'),
(12, 'anguraj79@rediffmail.com', NULL, NULL, '2021-02-23 21:33:41', '2021-02-23 21:33:41', '2021-02-23 21:34:11', 0, 'ajantaapdrops2811'),
(13, 'info@himak.in', NULL, NULL, '2021-02-24 09:16:29', '2021-03-09 08:45:37', '2021-03-09 08:59:10', 0, 'ajantaapdrops2811'),
(14, 'upendra.dabbukottu@siemens.com', NULL, NULL, '2021-02-25 15:56:13', '2021-03-11 14:01:06', '2021-03-11 16:08:06', 0, 'ajantaapdrops2811'),
(15, 'mukund.singh@siemens.com', NULL, NULL, '2021-02-25 15:56:53', '2021-02-25 15:56:53', '2021-02-25 15:57:23', 0, 'ajantaapdrops2811'),
(16, 'richa@automationsolutions.co.in', NULL, NULL, '2021-03-03 14:37:50', '2021-03-09 14:36:41', '2021-03-09 19:56:39', 0, 'ajantaapdrops2811'),
(17, 'nandakumar.narasimhan@siemens.com', NULL, NULL, '2021-03-04 00:09:14', '2021-03-09 15:57:24', '2021-03-09 16:09:55', 0, 'ajantaapdrops2811'),
(18, 'sujatha@coact.co.in', NULL, NULL, '2021-03-04 12:55:50', '2021-03-04 12:55:50', '2021-03-04 14:00:20', 0, 'ajantaapdrops2811'),
(19, 'tanvi.hegde@siemens.com', NULL, NULL, '2021-03-04 13:07:35', '2021-10-14 18:14:45', '2021-10-14 18:15:15', 1, 'ajantaapdrops2811'),
(20, 'yuvraj.nagarkar@siemens.com', NULL, NULL, '2021-03-04 13:07:50', '2021-03-09 13:54:18', '2021-03-09 14:08:19', 0, 'ajantaapdrops2811'),
(21, 'makwanakaluram@gmail.com', NULL, NULL, '2021-03-04 17:44:04', '2021-03-04 17:44:04', '2021-03-04 17:51:57', 0, 'ajantaapdrops2811'),
(22, 'technical@airflow.co.in', NULL, NULL, '2021-03-05 12:32:25', '2021-03-10 13:26:22', '2021-03-10 13:36:03', 0, 'ajantaapdrops2811'),
(23, 'patel.tejas@siemens.com', NULL, NULL, '2021-03-05 13:10:45', '2021-03-11 13:52:13', '2021-03-11 16:11:28', 0, 'ajantaapdrops2811'),
(24, 'umesh.shah@siemens.com', NULL, NULL, '2021-03-05 13:11:01', '2021-03-11 15:40:25', '2021-03-11 16:20:25', 0, 'ajantaapdrops2811'),
(25, 'vedavyas.nayak@siemens.com', NULL, NULL, '2021-03-05 13:11:21', '2021-03-11 22:42:14', '2021-03-11 22:42:44', 0, 'ajantaapdrops2811'),
(26, 'naveen.kumar@beumer.com', NULL, NULL, '2021-03-08 08:57:34', '2021-03-08 08:57:34', '2021-03-08 08:58:04', 0, 'ajantaapdrops2811'),
(27, 'sastryb@xpeedas.com', NULL, NULL, '2021-03-08 10:20:55', '2021-03-08 10:20:55', '2021-03-08 10:21:25', 0, 'ajantaapdrops2811'),
(28, 'marketing@aksheyaprojects.com', NULL, NULL, '2021-03-08 10:24:28', '2021-03-09 16:32:25', '2021-03-09 16:32:55', 0, 'ajantaapdrops2811'),
(29, 'bhprasad@hanumanautomation.com', NULL, NULL, '2021-03-08 10:27:04', '2021-03-08 10:27:04', '2021-03-08 10:27:34', 0, 'ajantaapdrops2811'),
(30, 'sharma.ps@siemens.com', NULL, NULL, '2021-03-08 10:31:40', '2021-03-09 14:12:02', '2021-03-09 14:19:33', 0, 'ajantaapdrops2811'),
(31, 'sawant.sarvesh@siemens.com', NULL, NULL, '2021-03-08 10:34:15', '2021-03-09 13:55:15', '2021-03-09 16:09:48', 0, 'ajantaapdrops2811'),
(32, 'ananthachari.sreeram@siemens.com', NULL, NULL, '2021-03-08 11:22:55', '2021-03-11 13:58:37', '2021-03-11 18:33:17', 0, 'ajantaapdrops2811'),
(33, 'pradeepsingh@siemens.com', NULL, NULL, '2021-03-08 18:58:34', '2021-03-09 13:58:42', '2021-03-09 16:09:39', 0, 'ajantaapdrops2811'),
(34, 'sangamesh.patil@trimasyscontrol.com', NULL, NULL, '2021-03-08 19:44:35', '2021-03-09 16:27:12', '2021-03-09 16:27:42', 0, 'ajantaapdrops2811'),
(35, 'raut.aditya@siemens.com', NULL, NULL, '2021-03-08 20:51:10', '2021-03-09 13:59:07', '2021-03-09 16:09:37', 0, 'ajantaapdrops2811'),
(36, 'rupesh.patil@trimasyscontrol.com', NULL, NULL, '2021-03-08 21:06:06', '2021-03-08 21:06:06', '2021-03-08 21:07:06', 0, 'ajantaapdrops2811'),
(37, 'sreenivasulu@hanumanautomation.com', NULL, NULL, '2021-03-08 21:09:48', '2021-03-08 21:09:48', '2021-03-08 21:10:18', 0, 'ajantaapdrops2811'),
(38, 'amitabha.mallick@siemens.com', NULL, NULL, '2021-03-08 21:16:38', '2021-03-09 16:57:12', '2021-03-09 16:57:42', 0, 'ajantaapdrops2811'),
(39, 'moiz.patanwala@siemens.com', NULL, NULL, '2021-03-08 22:14:29', '2021-03-09 14:03:48', '2021-03-09 15:02:19', 0, 'ajantaapdrops2811'),
(40, 'pankaj.kamdi@siemens.com', NULL, NULL, '2021-03-08 22:20:40', '2021-03-09 14:03:54', '2021-03-09 16:16:55', 0, 'ajantaapdrops2811'),
(41, 'vijayshankar.jadhav@siemens.com', NULL, NULL, '2021-03-08 22:35:36', '2021-03-09 14:01:36', '2021-03-09 16:10:07', 0, 'ajantaapdrops2811'),
(42, 'ritvik.balgi@siemens.com', NULL, NULL, '2021-03-08 22:53:35', '2021-03-08 22:53:35', '2021-03-08 23:49:07', 0, 'ajantaapdrops2811'),
(43, 'amit.berde@godrejkoerber.com', NULL, NULL, '2021-03-09 08:44:54', '2021-03-09 14:21:40', '2021-03-09 16:14:56', 0, 'ajantaapdrops2811'),
(44, 'amit.tripathi@cotmac.io', NULL, NULL, '2021-03-09 09:59:39', '2021-03-10 15:07:05', '2021-03-10 15:07:35', 0, 'ajantaapdrops2811'),
(45, 'rajakumar.k@siemens.com', NULL, NULL, '2021-03-09 10:32:04', '2021-03-09 14:04:30', '2021-03-09 16:10:39', 0, 'ajantaapdrops2811'),
(46, 'rajneesh.mahalaya@siemens.com', NULL, NULL, '2021-03-09 10:41:57', '2021-03-09 13:59:34', '2021-03-09 16:35:04', 0, 'ajantaapdrops2811'),
(47, 'rahul.unni@cotmac.io', NULL, NULL, '2021-03-09 10:44:41', '2021-03-09 10:44:41', '2021-03-09 10:45:11', 0, 'ajantaapdrops2811'),
(48, 'manoj.dharmadhikari@siemens.com', NULL, NULL, '2021-03-09 10:57:43', '2021-03-11 13:57:37', '2021-03-11 16:08:40', 0, 'ajantaapdrops2811'),
(49, 'abc@gmail.com', NULL, NULL, '2021-03-09 10:59:03', '2021-03-09 16:18:45', '2021-03-09 16:22:13', 0, 'ajantaapdrops2811'),
(50, 'shrikant.diwane@cotmac.io', NULL, NULL, '2021-03-09 11:09:30', '2021-03-09 11:09:30', '2021-03-09 11:11:03', 0, 'ajantaapdrops2811'),
(51, 'kmahesh@odincontrols.com', NULL, NULL, '2021-03-09 11:13:52', '2021-03-09 14:34:56', '2021-03-09 16:36:27', 0, 'ajantaapdrops2811'),
(52, 'aketkar@precimac.com', NULL, NULL, '2021-03-09 11:19:40', '2021-03-09 14:01:58', '2021-03-09 15:32:58', 0, 'ajantaapdrops2811'),
(53, 'nashik.sales@multiquadrant.com', NULL, NULL, '2021-03-09 11:28:21', '2021-03-09 13:54:47', '2021-03-09 16:10:47', 0, 'ajantaapdrops2811'),
(54, 'shashank.bhavsar@siemens.com', NULL, NULL, '2021-03-09 11:45:55', '2021-03-09 14:02:38', '2021-03-09 16:09:38', 0, 'ajantaapdrops2811'),
(55, 'gangeshwar.g@siemens.com', NULL, NULL, '2021-03-09 11:47:20', '2021-03-09 14:17:41', '2021-03-09 16:10:11', 0, 'ajantaapdrops2811'),
(56, 'prasad.kharche@siemens.com', NULL, NULL, '2021-03-09 11:56:11', '2021-03-09 14:32:28', '2021-03-09 16:25:06', 0, 'ajantaapdrops2811'),
(57, 'anshul.luthra@siemens.com', NULL, NULL, '2021-03-09 11:57:27', '2021-03-09 14:24:17', '2021-03-09 16:24:48', 0, 'ajantaapdrops2811'),
(58, 'satcon2006@gmail.com', NULL, NULL, '2021-03-09 11:57:45', '2021-03-09 11:57:45', '2021-03-09 11:58:15', 0, 'ajantaapdrops2811'),
(59, 'satcon200@gmail.com', NULL, NULL, '2021-03-09 11:57:54', '2021-03-09 11:57:54', '2021-03-09 11:58:24', 0, 'ajantaapdrops2811'),
(60, 'shivaji.kakade@siemens.com', NULL, NULL, '2021-03-09 11:58:02', '2021-03-09 14:01:23', '2021-03-09 17:07:26', 0, 'ajantaapdrops2811'),
(61, 'prabhav@nikkainc.com', NULL, NULL, '2021-03-09 12:01:06', '2021-03-09 12:01:06', '2021-03-09 12:06:37', 0, 'ajantaapdrops2811'),
(62, 'swapnil.hivarkar@trimasyscontrol.com', NULL, NULL, '2021-03-09 12:03:44', '2021-03-09 15:48:34', '2021-03-09 15:59:37', 0, 'ajantaapdrops2811'),
(63, 'alexkotoor@gmail.com', NULL, NULL, '2021-03-09 12:10:18', '2021-03-09 15:20:49', '2021-03-09 16:39:33', 0, 'ajantaapdrops2811'),
(64, 'purchase@nidoworld.com', NULL, NULL, '2021-03-09 12:10:47', '2021-03-09 14:13:42', '2021-03-09 14:45:12', 0, 'ajantaapdrops2811'),
(65, 'automationrealtime@gmail.com', NULL, NULL, '2021-03-09 12:11:30', '2021-03-11 14:00:16', '2021-03-11 15:00:47', 0, 'ajantaapdrops2811'),
(66, 'ajain@contrivekota.in', NULL, NULL, '2021-03-09 12:46:36', '2021-03-09 12:46:36', '2021-03-09 12:47:06', 0, 'ajantaapdrops2811'),
(67, 'aniket.kasat@foxindia.net', NULL, NULL, '2021-03-09 12:53:06', '2021-03-09 12:53:06', '2021-03-09 18:09:41', 0, 'ajantaapdrops2811'),
(68, 'rohan.pawade@trimasyscontrol.com', NULL, NULL, '2021-03-09 12:53:30', '2021-03-09 14:18:51', '2021-03-09 15:07:23', 0, 'ajantaapdrops2811'),
(69, 'subhash@sharplinegroup.com', NULL, NULL, '2021-03-09 12:57:05', '2021-03-09 12:57:05', '2021-03-09 12:57:35', 0, 'ajantaapdrops2811'),
(70, 'marketing@activesystemsindia.com', NULL, NULL, '2021-03-09 13:07:07', '2021-03-09 16:33:23', '2021-03-09 16:33:53', 0, 'ajantaapdrops2811'),
(71, 'avinash.gonsalves@siemens.com', NULL, NULL, '2021-03-09 13:11:37', '2021-03-09 14:21:41', '2021-03-09 16:14:13', 0, 'ajantaapdrops2811'),
(72, 'pi@nikkainc.com', NULL, NULL, '2021-03-09 13:11:50', '2021-03-09 13:11:50', '2021-03-09 16:58:53', 0, 'ajantaapdrops2811'),
(73, 'vinay.singh@armstrongltd.in', NULL, NULL, '2021-03-09 13:23:45', '2021-03-11 16:41:24', '2021-03-11 18:25:25', 0, 'ajantaapdrops2811'),
(74, 'anil@grupsautomation.co.in', NULL, NULL, '2021-03-09 13:33:41', '2021-03-09 13:57:32', '2021-03-09 15:13:03', 0, 'ajantaapdrops2811'),
(75, 'sayontoni.banerjee@siemens.com', NULL, NULL, '2021-03-09 13:45:52', '2021-03-09 13:45:52', '2021-03-09 16:17:24', 0, 'ajantaapdrops2811'),
(76, 'anand.rathi@pritiprojects.com', NULL, NULL, '2021-03-09 13:53:14', '2021-03-09 13:53:14', '2021-03-09 16:17:44', 0, 'ajantaapdrops2811'),
(77, 'pranav.rathod@siemens.com', NULL, NULL, '2021-03-09 13:54:37', '2021-03-09 13:54:37', '2021-03-09 16:09:39', 0, 'ajantaapdrops2811'),
(78, 'srhanmante@multiquadrant.com', NULL, NULL, '2021-03-09 13:54:40', '2021-03-09 14:24:23', '2021-03-09 16:58:44', 0, 'ajantaapdrops2811'),
(79, 'madhur.jagdale@siemens.com', NULL, NULL, '2021-03-09 13:56:05', '2021-03-09 13:56:05', '2021-03-09 16:26:20', 0, 'ajantaapdrops2811'),
(80, 'anup.abhyankar@siemens.com', NULL, NULL, '2021-03-09 13:56:39', '2021-03-09 14:05:00', '2021-03-09 16:10:00', 0, 'ajantaapdrops2811'),
(81, 'mukhtar.khan@siemens.com', NULL, NULL, '2021-03-09 13:56:53', '2021-03-09 13:58:39', '2021-03-09 15:54:09', 0, 'ajantaapdrops2811'),
(82, 'support@multiquadrant.com', NULL, NULL, '2021-03-09 13:57:06', '2021-03-09 13:57:06', '2021-03-09 16:11:37', 0, 'ajantaapdrops2811'),
(83, 'mitesh.sheth@siemens.com', NULL, NULL, '2021-03-09 13:58:03', '2021-03-09 13:58:03', '2021-03-09 23:10:04', 0, 'ajantaapdrops2811'),
(84, 'hemant.naik@foxindia.net', NULL, NULL, '2021-03-09 13:58:12', '2021-03-09 13:58:12', '2021-03-09 15:13:13', 0, 'ajantaapdrops2811'),
(85, 'aquibshaikh8976@gmail.com', NULL, NULL, '2021-03-09 13:58:32', '2021-03-09 14:09:40', '2021-03-09 16:33:33', 0, 'ajantaapdrops2811'),
(86, 'mounica.b@cotmac.io', NULL, NULL, '2021-03-09 13:58:37', '2021-03-09 13:58:37', '2021-03-09 16:10:38', 0, 'ajantaapdrops2811'),
(87, 'jithin.jacob@siemens.com', NULL, NULL, '2021-03-09 13:59:06', '2021-03-09 14:19:09', '2021-03-09 15:32:10', 0, 'ajantaapdrops2811'),
(88, 'aswini.b@cotmac.io', NULL, NULL, '2021-03-09 13:59:26', '2021-03-09 15:28:18', '2021-03-09 16:10:57', 0, 'ajantaapdrops2811'),
(89, 'kushaba.kokare@dacpl.in', NULL, NULL, '2021-03-09 13:59:36', '2021-03-09 15:22:32', '2021-03-09 16:54:04', 0, 'ajantaapdrops2811'),
(90, 'sales@multiquadrant.com', NULL, NULL, '2021-03-09 13:59:39', '2021-03-09 13:59:39', '2021-03-09 16:14:10', 0, 'ajantaapdrops2811'),
(91, 'Fabian.perner@siemens.com', NULL, NULL, '2021-03-09 14:00:04', '2021-03-09 14:00:04', '2021-03-09 16:09:36', 0, 'ajantaapdrops2811'),
(92, 'mohakmore@live.com', NULL, NULL, '2021-03-09 14:00:17', '2021-03-11 14:07:26', '2021-03-11 14:13:27', 0, 'ajantaapdrops2811'),
(93, 'dhanasekhar.maraka@siemens.com', NULL, NULL, '2021-03-09 14:00:19', '2021-03-09 14:29:13', '2021-03-09 16:09:47', 0, 'ajantaapdrops2811'),
(94, 'sahil.bhatia@siemens.com', NULL, NULL, '2021-03-09 14:00:22', '2021-03-09 14:00:22', '2021-03-09 16:12:57', 0, 'ajantaapdrops2811'),
(95, 'suraj.benni@siemens.com', NULL, NULL, '2021-03-09 14:00:33', '2021-03-09 14:00:33', '2021-03-09 14:05:34', 0, 'ajantaapdrops2811'),
(96, 'projects.support@multiquadrant.com', NULL, NULL, '2021-03-09 14:00:37', '2021-03-09 14:26:00', '2021-03-09 16:10:01', 0, 'ajantaapdrops2811'),
(97, 'alankar.manjrekar@siemens.com', NULL, NULL, '2021-03-09 14:00:42', '2021-03-09 15:37:22', '2021-03-09 16:10:24', 0, 'ajantaapdrops2811'),
(98, 'ho.support@multiquadrant.com', NULL, NULL, '2021-03-09 14:01:03', '2021-03-09 15:55:41', '2021-03-09 17:42:58', 0, 'ajantaapdrops2811'),
(99, 'rohit.khairnar@armstrongltd.in', NULL, NULL, '2021-03-09 14:01:19', '2021-03-09 14:01:19', '2021-03-09 14:04:50', 0, 'ajantaapdrops2811'),
(100, 'karthick.m@siemens.com', NULL, NULL, '2021-03-09 14:01:22', '2021-03-09 14:19:58', '2021-03-09 16:02:17', 0, 'ajantaapdrops2811'),
(101, 'manali.ashtaputre@siemens.com', NULL, NULL, '2021-03-09 14:01:23', '2021-03-09 14:03:56', '2021-03-09 16:18:58', 0, 'ajantaapdrops2811'),
(102, 'lokesh.kumar@nexgenindia.in', NULL, NULL, '2021-03-09 14:01:31', '2021-03-09 14:01:31', '2021-03-09 16:09:32', 0, 'ajantaapdrops2811'),
(103, 'anup.badave@siemens.com', NULL, NULL, '2021-03-09 14:01:50', '2021-03-09 14:01:50', '2021-03-09 16:20:21', 0, 'ajantaapdrops2811'),
(104, 'piyush.kumar.ext@siemens.com', NULL, NULL, '2021-03-09 14:02:33', '2021-03-09 14:02:33', '2021-03-09 16:13:42', 0, 'ajantaapdrops2811'),
(105, 'nishant.yadav@siemens.com', NULL, NULL, '2021-03-09 14:02:37', '2021-03-09 14:02:37', '2021-03-09 16:10:38', 0, 'ajantaapdrops2811'),
(106, 'monisha.ca@cotmac.io', NULL, NULL, '2021-03-09 14:02:52', '2021-03-09 14:02:52', '2021-03-09 15:59:22', 0, 'ajantaapdrops2811'),
(107, 'anand.deshmukh@ssi-schaefer.com', NULL, NULL, '2021-03-09 14:02:52', '2021-03-09 14:07:55', '2021-03-09 16:13:27', 0, 'ajantaapdrops2811'),
(108, 'aqsa@evio.in', NULL, NULL, '2021-03-09 14:02:55', '2021-03-09 16:06:26', '2021-03-09 16:06:56', 0, 'ajantaapdrops2811'),
(109, 'nikhil.dalvi@srsystems.in', NULL, NULL, '2021-03-09 14:03:22', '2021-03-09 14:03:22', '2021-03-09 14:45:25', 0, 'ajantaapdrops2811'),
(110, 'sagarika.yannuwar@siemens.com', NULL, NULL, '2021-03-09 14:03:32', '2021-03-09 16:12:21', '2021-03-09 16:38:22', 0, 'ajantaapdrops2811'),
(111, 'alokd@evio.in', NULL, NULL, '2021-03-09 14:03:33', '2021-03-09 15:03:13', '2021-03-09 16:02:14', 0, 'ajantaapdrops2811'),
(112, 'nair.rajesh@siemens.com', NULL, NULL, '2021-03-09 14:03:51', '2021-03-09 14:03:51', '2021-03-09 14:20:59', 0, 'ajantaapdrops2811'),
(113, 'sagar.shinde@armstrongltd.in', NULL, NULL, '2021-03-09 14:03:59', '2021-03-09 14:03:59', '2021-03-09 17:32:34', 0, 'ajantaapdrops2811'),
(114, 'sanjeevsingh@evio.in', NULL, NULL, '2021-03-09 14:04:53', '2021-03-09 15:10:21', '2021-03-09 16:09:22', 0, 'ajantaapdrops2811'),
(115, 'suhas@evio.in', NULL, NULL, '2021-03-09 14:04:58', '2021-03-09 15:15:55', '2021-03-09 16:10:25', 0, 'ajantaapdrops2811'),
(116, 'kairav.modi@siemens.com', NULL, NULL, '2021-03-09 14:05:12', '2021-03-09 14:06:52', '2021-03-09 15:45:52', 0, 'ajantaapdrops2811'),
(117, 'suraj.benni@simens.com', NULL, NULL, '2021-03-09 14:05:25', '2021-03-09 14:05:25', '2021-03-09 16:23:45', 0, 'ajantaapdrops2811'),
(118, 'roy.sanjib@siemens.com', NULL, NULL, '2021-03-09 14:05:47', '2021-03-09 14:05:47', '2021-03-09 15:29:47', 0, 'ajantaapdrops2811'),
(119, 'kendre.pradeep@siemens.com', NULL, NULL, '2021-03-09 14:06:11', '2021-03-09 14:06:11', '2021-03-09 15:13:43', 0, 'ajantaapdrops2811'),
(120, 'hadis.farhan@foxindia.net', NULL, NULL, '2021-03-09 14:06:33', '2021-03-09 14:42:01', '2021-03-09 16:00:39', 0, 'ajantaapdrops2811'),
(121, 'vikrant.babar@dacpl.in', NULL, NULL, '2021-03-09 14:07:27', '2021-03-10 13:57:26', '2021-03-10 14:20:04', 0, 'ajantaapdrops2811'),
(122, 'anand.gawade@siemens.com', NULL, NULL, '2021-03-09 14:07:54', '2021-03-09 16:18:29', '2021-03-09 16:19:29', 0, 'ajantaapdrops2811'),
(123, 'zodagerajesh@gmail.com', NULL, NULL, '2021-03-09 14:08:09', '2021-03-09 14:08:09', '2021-03-09 14:09:10', 0, 'ajantaapdrops2811'),
(124, 'ponnivalavan.p@siemens.com', NULL, NULL, '2021-03-09 14:09:08', '2021-03-09 14:09:08', '2021-03-09 15:54:54', 0, 'ajantaapdrops2811'),
(125, 'marketing2@activesystemsindia.com', NULL, NULL, '2021-03-09 14:09:43', '2021-03-09 14:17:56', '2021-03-09 16:14:57', 0, 'ajantaapdrops2811'),
(126, 'x@abc', NULL, NULL, '2021-03-09 14:09:48', '2021-03-09 14:09:48', '2021-03-09 15:00:32', 0, 'ajantaapdrops2811'),
(127, 'rajat.verma@cotmac.io', NULL, NULL, '2021-03-09 14:09:56', '2021-03-09 14:09:56', '2021-03-09 14:50:56', 0, 'ajantaapdrops2811'),
(128, 'S.kumar@mhitraa.com', NULL, NULL, '2021-03-09 14:10:04', '2021-03-09 15:06:23', '2021-03-09 16:09:53', 0, 'ajantaapdrops2811'),
(129, 'sonaukarde@gmail.com', NULL, NULL, '2021-03-09 14:10:08', '2021-03-09 14:45:35', '2021-03-09 14:53:06', 0, 'ajantaapdrops2811'),
(130, 'sales.automation@trimasyscontrol.com', NULL, NULL, '2021-03-09 14:10:23', '2021-03-09 14:15:34', '2021-03-09 19:33:53', 0, 'ajantaapdrops2811'),
(131, 'bala@activesystemsindia.com', NULL, NULL, '2021-03-09 14:10:53', '2021-03-09 14:10:53', '2021-03-09 16:21:23', 0, 'ajantaapdrops2811'),
(132, 'yogesh.thakare@siemens.com', NULL, NULL, '2021-03-09 14:11:15', '2021-03-09 14:11:15', '2021-03-09 16:40:19', 0, 'ajantaapdrops2811'),
(133, 'rahultambe03@gmail.com', NULL, NULL, '2021-03-09 14:12:52', '2021-03-09 15:07:36', '2021-03-09 16:12:37', 0, 'ajantaapdrops2811'),
(134, 'k.kundan@schenckprocess.com', NULL, NULL, '2021-03-09 14:13:12', '2021-03-09 14:14:10', '2021-03-09 15:03:41', 0, 'ajantaapdrops2811'),
(135, 'deepshikhar.shukla@siemens.com', NULL, NULL, '2021-03-09 14:14:07', '2021-03-09 14:14:07', '2021-03-10 09:44:52', 0, 'ajantaapdrops2811'),
(136, 'amolchaudhari96@gmail.com', NULL, NULL, '2021-03-09 14:14:35', '2021-03-09 16:33:03', '2021-03-09 16:33:33', 0, 'ajantaapdrops2811'),
(137, 'punam.mandve@trimasyscontrol.com', NULL, NULL, '2021-03-09 14:15:46', '2021-03-09 14:15:46', '2021-03-09 17:11:38', 0, 'ajantaapdrops2811'),
(138, 'kumar.prem@siemens.com', NULL, NULL, '2021-03-09 14:17:11', '2021-03-09 14:43:26', '2021-03-09 15:36:57', 0, 'ajantaapdrops2811'),
(139, 'vedavyas.nayak@siemens', NULL, NULL, '2021-03-09 14:18:15', '2021-03-09 14:18:15', '2021-03-09 14:18:45', 0, 'ajantaapdrops2811'),
(140, 'vikash_kumar.singh@siemens.com', NULL, NULL, '2021-03-09 14:19:19', '2021-03-09 14:19:19', '2021-03-09 16:19:50', 0, 'ajantaapdrops2811'),
(141, 'pankaj.raut@cotmac.io', NULL, NULL, '2021-03-09 14:20:24', '2021-03-09 14:20:24', '2021-03-09 15:38:25', 0, 'ajantaapdrops2811'),
(142, 'SOUMYA.CHATTERJEE@SIEMENS.COM', NULL, NULL, '2021-03-09 14:21:46', '2021-03-09 15:58:54', '2021-03-09 17:21:38', 0, 'ajantaapdrops2811'),
(143, 'prabhakar.kumar@foxindia.net', NULL, NULL, '2021-03-09 14:23:27', '2021-03-09 14:39:58', '2021-03-09 14:57:51', 0, 'ajantaapdrops2811'),
(144, 'nikhil.thakre@ril.com', NULL, NULL, '2021-03-09 14:23:54', '2021-03-09 14:29:27', '2021-03-09 15:48:58', 0, 'ajantaapdrops2811'),
(145, 'khelan.mehta@foxindia.net', NULL, NULL, '2021-03-09 14:24:12', '2021-03-09 14:34:36', '2021-03-09 14:58:09', 0, 'ajantaapdrops2811'),
(146, 'kroopak@evio.in', NULL, NULL, '2021-03-09 14:25:41', '2021-03-09 14:25:41', '2021-03-09 16:05:59', 0, 'ajantaapdrops2811'),
(147, 'shankar.bansal@cotmac.io', NULL, NULL, '2021-03-09 14:25:46', '2021-03-09 14:25:46', '2021-03-09 14:34:17', 0, 'ajantaapdrops2811'),
(148, 'sidhesh.parab@invotec.in', NULL, NULL, '2021-03-09 14:26:41', '2021-03-09 14:26:41', '2021-03-09 15:45:12', 0, 'ajantaapdrops2811'),
(149, 'akshay.gaidhane@cotmac.io', NULL, NULL, '2021-03-09 14:27:33', '2021-03-09 14:27:33', '2021-03-09 17:04:10', 0, 'ajantaapdrops2811'),
(150, 'hari@activesystemsindia.com', NULL, NULL, '2021-03-09 14:27:41', '2021-03-09 14:27:41', '2021-03-09 16:09:47', 0, 'ajantaapdrops2811'),
(151, 'umesh.kumar@cotmac.io', NULL, NULL, '2021-03-09 14:28:30', '2021-03-09 14:28:30', '2021-03-09 14:31:00', 0, 'ajantaapdrops2811'),
(152, 'arbmelectrical@gmail.com', NULL, NULL, '2021-03-09 14:28:31', '2021-03-09 15:05:56', '2021-03-09 19:39:31', 0, 'ajantaapdrops2811'),
(153, 'narender.singh@falconautoonline.com', NULL, NULL, '2021-03-09 14:29:07', '2021-03-09 14:29:07', '2021-03-09 19:10:57', 0, 'ajantaapdrops2811'),
(154, 'Abhinav.bansal@siemens.com', NULL, NULL, '2021-03-09 14:30:07', '2021-03-09 15:31:44', '2021-03-09 15:40:46', 0, 'ajantaapdrops2811'),
(155, 'umesh.pal@siemens.com', NULL, NULL, '2021-03-09 14:36:05', '2021-03-09 14:36:05', '2021-03-09 16:10:26', 0, 'ajantaapdrops2811'),
(156, 'rajesh.thote@siemens.com', NULL, NULL, '2021-03-09 14:36:20', '2021-03-09 14:36:20', '2021-03-09 16:09:43', 0, 'ajantaapdrops2811'),
(157, 'anshul.keche@siemens.com', NULL, NULL, '2021-03-09 14:36:55', '2021-03-09 15:45:41', '2021-03-09 15:56:41', 0, 'ajantaapdrops2811'),
(158, 'pandharebhushan@gmail.com', NULL, NULL, '2021-03-09 14:37:46', '2021-03-09 14:37:46', '2021-03-09 16:09:46', 0, 'ajantaapdrops2811'),
(159, 'arpit.agarwal@siemens.com', NULL, NULL, '2021-03-09 14:39:29', '2021-03-09 14:39:29', '2021-03-09 16:33:31', 0, 'ajantaapdrops2811'),
(160, 'satish.konnur@ril.com', NULL, NULL, '2021-03-09 14:40:28', '2021-03-09 14:40:28', '2021-03-09 14:42:30', 0, 'ajantaapdrops2811'),
(161, 'helenjudit@gmail.com', NULL, NULL, '2021-03-09 14:41:19', '2021-03-09 14:41:19', '2021-03-09 17:11:50', 0, 'ajantaapdrops2811'),
(162, 'bhaskar.mandal@siemens.com', NULL, NULL, '2021-03-09 14:44:11', '2021-03-09 14:44:11', '2021-03-09 15:22:11', 0, 'ajantaapdrops2811'),
(163, 'kpsingh@tejcontrol.com', NULL, NULL, '2021-03-09 14:44:56', '2021-03-09 14:44:56', '2021-03-09 16:50:26', 0, 'ajantaapdrops2811'),
(164, 'vinaykumar.sharma@siemens.com', NULL, NULL, '2021-03-09 14:45:28', '2021-03-09 14:45:28', '2021-03-09 15:31:27', 0, 'ajantaapdrops2811'),
(165, 'janhavi.sonone@trimasyscontrol.com', NULL, NULL, '2021-03-09 14:46:19', '2021-03-09 15:04:12', '2021-03-09 16:13:42', 0, 'ajantaapdrops2811'),
(166, 'arc@cnautomation.in', NULL, NULL, '2021-03-09 14:47:17', '2021-03-09 14:47:17', '2021-03-09 16:11:48', 0, 'ajantaapdrops2811'),
(167, 'nagesh@texcel.in', NULL, NULL, '2021-03-09 14:47:48', '2021-03-09 14:47:48', '2021-03-09 15:21:18', 0, 'ajantaapdrops2811'),
(168, 'rajesh.likhitkar@siemens.com', NULL, NULL, '2021-03-09 15:30:31', '2021-03-09 15:30:31', '2021-03-09 15:32:01', 0, 'ajantaapdrops2811'),
(169, 'vinay.singh@siemens.com', NULL, NULL, '2021-03-09 15:31:57', '2021-03-09 15:31:57', '2021-03-09 16:12:58', 0, 'ajantaapdrops2811'),
(170, 'prem.jagadeesan@siemens.com', NULL, NULL, '2021-03-09 15:34:38', '2021-03-09 15:34:38', '2021-03-09 16:12:48', 0, 'ajantaapdrops2811'),
(171, 'tanvihegde@siemens.com', NULL, NULL, '2021-03-09 15:40:35', '2021-03-09 15:40:35', '2021-03-09 15:44:36', 0, 'ajantaapdrops2811'),
(172, 'grups@grupsautomation.in', NULL, NULL, '2021-03-09 15:45:06', '2021-03-09 15:45:06', '2021-03-09 16:10:23', 0, 'ajantaapdrops2811'),
(173, 'vishal.surve@cotmac.io', NULL, NULL, '2021-03-09 15:58:32', '2021-03-09 15:58:32', '2021-03-09 16:10:32', 0, 'ajantaapdrops2811'),
(174, 'ho@multiquadrant.com', NULL, NULL, '2021-03-09 16:09:58', '2021-03-09 16:09:58', '2021-03-09 16:16:28', 0, 'ajantaapdrops2811'),
(175, 'meet.shah@nidomachineries.in', NULL, NULL, '2021-03-09 16:51:53', '2021-03-09 16:51:53', '2021-03-09 16:52:23', 0, 'ajantaapdrops2811'),
(176, 'anamitra.majumdar@siemens.com', NULL, NULL, '2021-03-09 17:07:55', '2021-03-11 13:50:53', '2021-03-11 15:37:24', 0, 'ajantaapdrops2811'),
(177, 'aan@cnautomation.in', NULL, NULL, '2021-03-09 19:51:11', '2021-03-09 19:51:11', '2021-03-09 19:53:42', 0, 'ajantaapdrops2811'),
(178, 'abc@x.com', NULL, NULL, '2021-03-09 20:51:49', '2021-03-09 20:51:49', '2021-03-09 20:55:33', 0, 'ajantaapdrops2811'),
(179, 'hdhe@bdn.com', NULL, NULL, '2021-03-10 00:19:06', '2021-03-10 00:19:06', '2021-03-10 00:19:36', 0, 'ajantaapdrops2811'),
(180, 'taranjit.singh@addverb.in', NULL, NULL, '2021-03-10 10:52:11', '2021-03-10 10:52:11', '2021-03-10 12:21:23', 0, 'ajantaapdrops2811'),
(181, 'patel.jignesh@siemens.com', NULL, NULL, '2021-03-10 11:09:54', '2021-03-10 11:09:54', '2021-03-10 11:11:26', 0, 'ajantaapdrops2811'),
(182, 'satish.chandra@beumer.com', NULL, NULL, '2021-03-10 13:39:27', '2021-03-10 13:39:27', '2021-03-10 13:39:57', 0, 'ajantaapdrops2811'),
(183, 'service@idsautomation.in', NULL, NULL, '2021-03-10 14:07:33', '2021-03-11 14:14:57', '2021-03-11 17:18:57', 0, 'ajantaapdrops2811'),
(184, 'sasikumar@gennautomation.com', NULL, NULL, '2021-03-10 17:17:38', '2021-03-10 17:17:38', '2021-03-10 17:25:11', 0, 'ajantaapdrops2811'),
(185, 'siemensup@gmail.com', NULL, NULL, '2021-03-11 07:15:18', '2021-03-11 07:15:18', '2021-03-11 07:15:48', 0, 'ajantaapdrops2811'),
(186, 'chandrasekaran_j@wilcarwheelsltd.com', NULL, NULL, '2021-03-11 08:13:03', '2021-03-11 08:13:03', '2021-03-11 08:15:04', 0, 'ajantaapdrops2811'),
(187, 'kcpelectricalvyr@gmail.com', NULL, NULL, '2021-03-11 09:16:39', '2021-03-11 09:16:39', '2021-03-11 09:29:54', 0, 'ajantaapdrops2811'),
(188, 'yuvrajbalotiya@gmail.com', NULL, NULL, '2021-03-11 09:22:36', '2021-03-11 13:51:00', '2021-03-11 16:08:31', 0, 'ajantaapdrops2811'),
(189, 'dipak.bhor@adigitec-india.com', NULL, NULL, '2021-03-11 09:41:37', '2021-03-11 14:11:37', '2021-03-11 16:08:38', 0, 'ajantaapdrops2811'),
(190, 'sales.hwr2@cotmac.io', NULL, NULL, '2021-03-11 09:56:14', '2021-03-11 14:55:18', '2021-03-11 16:17:03', 0, 'ajantaapdrops2811'),
(191, 'prathish.t_m@siemens.com', NULL, NULL, '2021-03-11 11:10:33', '2021-03-11 11:10:33', '2021-03-11 15:59:35', 0, 'ajantaapdrops2811'),
(192, 'sunil@spautomation.net', NULL, NULL, '2021-03-11 11:11:10', '2021-03-11 14:53:16', '2021-03-11 16:07:32', 0, 'ajantaapdrops2811'),
(193, 'dhananjay.wagh@in.nestle.com', NULL, NULL, '2021-03-11 11:14:25', '2021-03-11 11:14:25', '2021-03-11 13:58:47', 0, 'ajantaapdrops2811'),
(194, 'tapeshwer@dreamzautomation.com', NULL, NULL, '2021-03-11 11:43:10', '2021-03-11 11:43:10', '2021-03-11 14:06:07', 0, 'ajantaapdrops2811'),
(195, 'amuthabalan.k@hirotecindia.com', NULL, NULL, '2021-03-11 12:23:58', '2021-03-11 15:03:06', '2021-03-11 16:07:37', 0, 'ajantaapdrops2811'),
(196, 'shivaji.salunkhe@addverb.in', NULL, NULL, '2021-03-11 12:45:46', '2021-03-11 14:13:07', '2021-03-11 14:50:37', 0, 'ajantaapdrops2811'),
(197, 'keerthan@symbiotic-systems.com', NULL, NULL, '2021-03-11 12:58:10', '2021-03-11 14:42:59', '2021-03-11 17:14:13', 0, 'ajantaapdrops2811'),
(198, 'rajendraa.dr@siemens.com', NULL, NULL, '2021-03-11 13:46:04', '2021-03-11 14:04:53', '2021-03-11 21:51:01', 0, 'ajantaapdrops2811'),
(199, 'sales@classicautomation.in', NULL, NULL, '2021-03-11 13:46:18', '2021-03-11 13:46:18', '2021-03-11 16:42:00', 0, 'ajantaapdrops2811'),
(200, 'rohit@classicautomation.in', NULL, NULL, '2021-03-11 13:51:51', '2021-03-11 13:51:51', '2021-03-11 16:39:08', 0, 'ajantaapdrops2811'),
(201, 'sagar.kamble@siemens.com', NULL, NULL, '2021-03-11 13:52:17', '2021-03-11 14:53:01', '2021-03-11 14:57:03', 0, 'ajantaapdrops2811'),
(202, 'vibhor.mehta@siemens.com', NULL, NULL, '2021-03-11 13:55:05', '2021-03-11 13:55:05', '2021-03-11 14:26:35', 0, 'ajantaapdrops2811'),
(203, 'vundavally.madanmohan@siemens.com', NULL, NULL, '2021-03-11 13:55:51', '2021-03-11 13:57:14', '2021-03-11 14:59:45', 0, 'ajantaapdrops2811'),
(204, 'panel@classicautomation.in', NULL, NULL, '2021-03-11 13:56:57', '2021-03-11 13:56:57', '2021-03-11 16:42:05', 0, 'ajantaapdrops2811'),
(205, 'yogesh.chaudhary@siemens.com', NULL, NULL, '2021-03-11 13:57:31', '2021-03-11 16:31:45', '2021-03-11 16:32:15', 0, 'ajantaapdrops2811'),
(206, 'services@classicautomation.in', NULL, NULL, '2021-03-11 13:57:42', '2021-03-11 14:39:42', '2021-03-11 15:22:02', 0, 'ajantaapdrops2811'),
(207, 'vineet.musale@siemens.com', NULL, NULL, '2021-03-11 13:58:41', '2021-03-11 13:58:41', '2021-03-11 15:33:11', 0, 'ajantaapdrops2811'),
(208, 'vijaypratap.singh@siemens.com', NULL, NULL, '2021-03-11 13:59:42', '2021-03-11 13:59:42', '2021-03-11 14:59:43', 0, 'ajantaapdrops2811'),
(209, 'x@abc.com', NULL, NULL, '2021-03-11 14:00:46', '2021-03-11 14:00:46', '2021-03-11 16:15:16', 0, 'ajantaapdrops2811'),
(210, 'shyam.sirur@cotmac.io', NULL, NULL, '2021-03-11 14:00:52', '2021-03-11 14:00:52', '2021-03-11 14:59:52', 0, 'ajantaapdrops2811'),
(211, 'chiranjit.ghara@addverb.in', NULL, NULL, '2021-03-11 14:01:28', '2021-03-11 14:01:28', '2021-03-11 15:02:58', 0, 'ajantaapdrops2811'),
(212, 'prashanth.karanth@siemens.com', NULL, NULL, '2021-03-11 14:02:29', '2021-03-11 14:29:30', '2021-03-11 15:18:46', 0, 'ajantaapdrops2811'),
(213, 'naresh.g@cotmac.io', NULL, NULL, '2021-03-11 14:03:22', '2021-03-11 14:57:15', '2021-03-11 15:28:27', 0, 'ajantaapdrops2811'),
(214, 'h.kandpal@classicautomation.in', NULL, NULL, '2021-03-11 14:04:04', '2021-03-11 14:04:04', '2021-03-11 15:08:35', 0, 'ajantaapdrops2811'),
(215, 'kartik.mehta@siemens.com', NULL, NULL, '2021-03-11 14:04:14', '2021-03-11 14:04:14', '2021-03-11 15:20:44', 0, 'ajantaapdrops2811'),
(216, 'ambrish.gupta@siemens.com', NULL, NULL, '2021-03-11 14:04:22', '2021-03-11 14:04:22', '2021-03-11 16:14:33', 0, 'ajantaapdrops2811'),
(217, 'karla.velarde@siemens.com', NULL, NULL, '2021-03-11 14:04:56', '2021-03-20 22:58:05', '2021-03-20 23:03:37', 0, 'ajantaapdrops2811'),
(218, 'chandrasekhar.kurra@siemens.com', NULL, NULL, '2021-03-11 14:05:25', '2021-03-11 14:05:25', '2021-03-11 15:05:35', 0, 'ajantaapdrops2811'),
(219, 'shijil.kv@madox.in', NULL, NULL, '2021-03-11 14:06:39', '2021-03-11 15:32:52', '2021-03-11 16:08:52', 0, 'ajantaapdrops2811'),
(220, 'surjeeth.pl@siemens.com', NULL, NULL, '2021-03-11 14:07:42', '2021-03-11 14:07:42', '2021-03-11 14:40:16', 0, 'ajantaapdrops2811'),
(221, 'ramasagar.kandregula@siemens.com', NULL, NULL, '2021-03-11 14:08:27', '2021-03-11 14:08:27', '2021-03-11 16:07:59', 0, 'ajantaapdrops2811'),
(222, 'office@classicautomation.in', NULL, NULL, '2021-03-11 14:08:38', '2021-03-11 14:08:38', '2021-03-11 16:42:10', 0, 'ajantaapdrops2811'),
(223, 'basavaraj.kiresur@krones.in', NULL, NULL, '2021-03-11 14:08:39', '2021-03-11 14:08:39', '2021-03-11 15:39:10', 0, 'ajantaapdrops2811'),
(224, 'tiwari.n.rishi@gmail.com', NULL, NULL, '2021-03-11 14:10:22', '2021-03-11 14:25:17', '2021-03-11 14:33:18', 0, 'ajantaapdrops2811'),
(225, 'abhijeet.kulkarni@krones.in', NULL, NULL, '2021-03-11 14:11:45', '2021-03-11 14:11:45', '2021-03-11 16:55:13', 0, 'ajantaapdrops2811'),
(226, 'samson.samuel@siemens.com', NULL, NULL, '2021-03-11 14:12:01', '2021-03-11 14:12:01', '2021-03-11 18:54:32', 0, 'ajantaapdrops2811'),
(227, 'joy.aloor@foxindia.net', NULL, NULL, '2021-03-11 14:12:57', '2021-03-11 14:12:57', '2021-03-11 15:52:28', 0, 'ajantaapdrops2811'),
(228, 'tdd_1@craftsmanautomation.com', NULL, NULL, '2021-03-11 14:13:11', '2021-03-11 14:13:11', '2021-03-11 15:42:46', 0, 'ajantaapdrops2811'),
(229, 'pranit.yadav@addverb.in', NULL, NULL, '2021-03-11 14:16:51', '2021-03-11 14:21:56', '2021-03-11 15:58:57', 0, 'ajantaapdrops2811'),
(230, 'harun.sabir@ats-group.com', NULL, NULL, '2021-03-11 14:18:54', '2021-03-11 15:13:03', '2021-03-11 16:08:34', 0, 'ajantaapdrops2811'),
(231, 'shiv@beetaconveyors.com', NULL, NULL, '2021-03-11 14:24:41', '2021-03-11 14:28:12', '2021-03-11 16:11:12', 0, 'ajantaapdrops2811'),
(232, 'balasubramanya.srikantaiah@krones.in', NULL, NULL, '2021-03-11 14:26:43', '2021-03-11 14:26:43', '2021-03-11 15:27:43', 0, 'ajantaapdrops2811'),
(233, 'arjun_s@craftsmanautomation.com', NULL, NULL, '2021-03-11 14:27:57', '2021-03-11 14:27:57', '2021-03-11 14:46:27', 0, 'ajantaapdrops2811'),
(234, 'ramesh.g@siemens.com', NULL, NULL, '2021-03-11 15:22:13', '2021-03-11 15:22:13', '2021-03-11 16:08:14', 0, 'ajantaapdrops2811'),
(235, 'sales@aeab.in', NULL, NULL, '2021-03-11 15:24:47', '2021-03-11 15:24:47', '2021-03-11 16:09:48', 0, 'ajantaapdrops2811'),
(236, 'ananth.vattikonda@gmail.com', NULL, NULL, '2021-03-11 16:09:05', '2021-03-11 16:09:05', '2021-03-11 16:10:36', 0, 'ajantaapdrops2811'),
(237, 'raja_mayur@yahoo.in', NULL, NULL, '2021-03-11 16:47:46', '2021-03-11 16:47:46', '2021-03-11 19:00:57', 0, 'ajantaapdrops2811'),
(238, 'pawan.kumar@armstrongltd.in', NULL, NULL, '2021-03-12 15:30:49', '2021-03-12 15:30:49', '2021-03-12 15:31:49', 0, 'ajantaapdrops2811');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=239;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
