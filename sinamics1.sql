-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 21, 2022 at 10:56 AM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sinamics1`
--

-- --------------------------------------------------------

--
-- Table structure for table `master_leaderboard`
--

CREATE TABLE `master_leaderboard` (
  `SESSION_ID` int(11) NOT NULL,
  `EMAIL_ID` varchar(255) NOT NULL,
  `USERNAME` varchar(255) NOT NULL,
  `SCORE` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_leaderboard`
--

INSERT INTO `master_leaderboard` (`SESSION_ID`, `EMAIL_ID`, `USERNAME`, `SCORE`) VALUES
(1, 'kamlesh@gmail.com', 'aj', 11),
(4, 'ash@mail.com', 'ashley', 800);

-- --------------------------------------------------------

--
-- Table structure for table `scores`
--

CREATE TABLE `scores` (
  `SCORE_ID` int(11) NOT NULL,
  `SESSION_ID` int(11) NOT NULL,
  `GAME_ID` int(11) NOT NULL,
  `EMAIL_ID` varchar(255) NOT NULL,
  `USERNAME` varchar(255) NOT NULL,
  `SCORE` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scores`
--

INSERT INTO `scores` (`SCORE_ID`, `SESSION_ID`, `GAME_ID`, `EMAIL_ID`, `USERNAME`, `SCORE`) VALUES
(2, 1, 1, 'kamlesh@gmail.com', 'aj', 6),
(3, 1, 2, 'kamlesh@gmail.com', 'aj', 5),
(4, 4, 1, 'ash@mail.com', 'ashley', 800);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_email`, `username`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(1, 'powashley74@gmail.com', 'ashley', NULL, '2021-06-24 19:07:15', '2021-06-24 19:07:15', '2021-06-24 19:46:39', 0, 'ajantaapdrops2811'),
(2, 'pawan@coact.co.in', 'PAWAN ', NULL, '2021-06-24 19:08:24', '2021-06-25 12:08:28', '2021-06-25 12:13:29', 0, 'ajantaapdrops2811'),
(3, 'powashley74@gmail.com', 'ashleypow', NULL, '2021-06-24 19:08:34', '2021-06-30 13:27:07', '2021-06-30 13:27:37', 0, 'ajantaapdrops2811'),
(4, 'rakshith@neosoulsoft.com', 'rakshith', NULL, '2021-06-24 19:16:29', '2021-06-24 19:26:38', '2021-06-24 19:27:08', 0, 'ajantaapdrops2811'),
(5, 'sandypagaria@gmail.com', 'sandypagaria@gmail.com', NULL, '2021-06-24 19:17:58', '2021-06-25 12:11:01', '2021-06-25 12:11:31', 0, 'ajantaapdrops2811'),
(6, 'rakshith@neosoulsoft.com', 'rakshith shetty', NULL, '2021-06-24 19:27:07', '2021-06-24 19:27:07', '2021-06-24 19:44:05', 0, 'ajantaapdrops2811'),
(7, 'mahesh@neosoulsoft.com', 'mahesh', NULL, '2021-06-24 19:29:55', '2021-06-24 19:38:49', '2021-06-24 19:48:42', 0, 'ajantaapdrops2811'),
(8, 'mahesh.p@neosoulsoft.com', 'mahesh', NULL, '2021-06-24 19:32:50', '2021-06-24 19:38:14', '2021-06-24 19:38:52', 0, 'ajantaapdrops2811'),
(9, 'a@mail.com', 'ashley2', NULL, '2021-06-24 19:41:50', '2021-06-24 19:41:50', '2021-06-24 19:44:42', 0, 'ajantaapdrops2811'),
(10, 'rakshithshetty32@gmail.com', 'Neosoulsoft', NULL, '2021-06-24 19:43:44', '2021-06-25 11:45:18', '2021-06-25 11:45:48', 0, 'ajantaapdrops2811'),
(11, 'ashley@gmail.com', 'ashley', NULL, '2021-06-24 19:44:39', '2021-06-24 19:44:39', '2021-06-24 19:50:48', 0, 'ajantaapdrops2811'),
(12, 'pagariapow@gmail.com', 'pagariapow', NULL, '2021-06-24 19:44:53', '2021-06-24 19:44:53', '2021-06-24 19:47:55', 0, 'ajantaapdrops2811'),
(13, 'rakshithshetty32@gmail.com', 'rakshith@nesoulsoft.com', NULL, '2021-06-24 19:45:30', '2021-06-24 19:45:30', '2021-06-24 20:11:35', 0, 'ajantaapdrops2811'),
(14, 'ash@gmail.com', 'ash', NULL, '2021-06-24 19:45:51', '2021-06-24 19:45:51', '2021-06-24 19:47:21', 0, 'ajantaapdrops2811'),
(15, 'mpatil2005@gmail.com', 'test', NULL, '2021-06-24 19:46:18', '2021-06-24 19:46:18', '2021-06-24 19:48:34', 0, 'ajantaapdrops2811'),
(16, 'pooja@coact.co.in', 'Pooja', NULL, '2021-06-24 19:46:42', '2021-07-26 18:47:27', '2021-07-26 18:48:29', 1, 'ajantaapdrops2811'),
(17, 'mpatil2006@gmail.com', 'mahesh', NULL, '2021-06-24 19:49:53', '2021-06-24 19:49:53', '2021-06-24 19:50:23', 0, 'ajantaapdrops2811'),
(18, 'nishanth@coact.co.in', 'Nishu', NULL, '2021-06-24 19:54:39', '2021-06-24 19:54:39', '2021-06-24 19:55:09', 0, 'ajantaapdrops2811'),
(19, 'nishanth@coact.co.in', 'Nishanth', NULL, '2021-06-24 19:55:01', '2021-06-24 19:55:12', '2021-06-24 19:55:42', 0, 'ajantaapdrops2811'),
(20, 'neeraj@coact.co.in', 'Coact_DevTeam', NULL, '2021-06-24 20:23:18', '2021-06-30 16:47:16', '2021-06-30 23:25:00', 0, 'ajantaapdrops2811'),
(21, 'pawan@coact.co.in', 'neerajreddy', NULL, '2021-06-24 20:29:27', '2021-06-24 20:29:27', '2021-06-24 20:52:10', 0, 'ajantaapdrops2811'),
(22, 'pandurevanthreddy002@gmail.com', 'neerajreddy', NULL, '2021-06-24 20:29:59', '2021-06-24 20:29:59', '2021-06-24 20:44:06', 0, 'ajantaapdrops2811'),
(23, 'neeraj@coact.co.in', 'neerajreddy', NULL, '2021-06-24 20:43:58', '2021-06-25 12:13:48', '2021-06-25 17:11:55', 0, 'ajantaapdrops2811'),
(24, 'pawan@coact.co.in', 'Coact_DevTeam', NULL, '2021-06-24 20:44:26', '2021-06-24 20:44:26', '2021-06-24 20:46:06', 0, 'ajantaapdrops2811'),
(25, 'au@mail.com', 'ash2', NULL, '2021-06-24 20:50:19', '2021-06-24 20:50:19', '2021-06-24 21:30:01', 0, 'ajantaapdrops2811'),
(26, 'kamlesh.071198@gmail.com', 'kamlesh', NULL, '2021-06-24 20:56:12', '2021-06-26 14:48:05', '2021-06-26 14:53:05', 0, 'ajantaapdrops2811'),
(27, 'viraj@coact.co.in', 'Coact_DevTeam', NULL, '2021-06-24 21:01:27', '2021-06-24 21:01:27', '2021-06-24 21:02:36', 0, 'ajantaapdrops2811'),
(28, 'viraj@coact.co.in', 'viraj', NULL, '2021-06-24 21:02:13', '2021-06-24 21:02:13', '2021-06-24 21:06:06', 0, 'ajantaapdrops2811'),
(29, 'pandurevanthreddy002@gmail.com', 'pandu', NULL, '2021-06-24 21:05:50', '2021-06-24 21:05:50', '2021-06-24 21:47:36', 0, 'ajantaapdrops2811'),
(30, 'adarsh@gmail.com', 'adarshco', NULL, '2021-06-24 21:08:10', '2021-06-24 21:08:10', '2021-06-24 21:08:40', 0, 'ajantaapdrops2811'),
(31, 'adarsh@gmail.com', 'adarshco77', NULL, '2021-06-24 21:08:34', '2021-06-24 21:08:34', '2021-06-24 21:14:04', 0, 'ajantaapdrops2811'),
(32, 'aditi@gmail.com', 'aditi', NULL, '2021-06-24 21:14:23', '2021-06-24 21:14:23', '2021-06-24 21:15:23', 0, 'ajantaapdrops2811'),
(33, 'pag@gmail.com', 'pagariapow', NULL, '2021-06-24 21:15:16', '2021-06-24 21:15:16', '2021-06-24 21:16:16', 0, 'ajantaapdrops2811'),
(34, 'ad@gmail.com', 'ad', NULL, '2021-06-24 21:16:07', '2021-06-24 21:16:07', '2021-06-24 21:30:08', 0, 'ajantaapdrops2811'),
(35, 'admin@gmail.com', 'admin', NULL, '2021-06-25 09:20:38', '2021-06-25 12:12:09', '2021-06-25 12:13:12', 0, 'ajantaapdrops2811'),
(36, 'master@gmail.com', 'master', NULL, '2021-06-25 09:21:21', '2021-06-25 09:21:21', '2021-06-25 09:34:44', 0, 'ajantaapdrops2811'),
(37, 'test@gmail.com', 'test', NULL, '2021-06-25 09:29:31', '2021-06-25 09:29:31', '2021-06-25 09:30:01', 0, 'ajantaapdrops2811'),
(38, 'support@neosoul.com', 'superadmin', NULL, '2021-06-25 09:32:43', '2021-06-25 09:32:43', '2021-06-25 09:33:13', 0, 'ajantaapdrops2811'),
(39, 'ss@gmail.com', 'ss', NULL, '2021-06-25 09:34:38', '2021-06-25 09:34:38', '2021-06-25 09:36:09', 0, 'ajantaapdrops2811'),
(40, 'sss@gmail.com', 'sss', NULL, '2021-06-25 09:36:25', '2021-06-25 09:36:25', '2021-06-25 09:37:27', 0, 'ajantaapdrops2811'),
(41, 'ddddddd@gmail.com', 'admin', NULL, '2021-06-25 09:37:21', '2021-06-25 09:38:19', '2021-06-25 09:40:19', 0, 'ajantaapdrops2811'),
(42, 'rahul@gmail.com', 'rahul', NULL, '2021-06-25 09:57:15', '2021-06-25 09:57:45', '2021-06-25 10:00:33', 0, 'ajantaapdrops2811'),
(43, 'mcdonalds@gmail.com', 'Neosoulsoft', NULL, '2021-06-25 10:00:22', '2021-06-25 10:00:22', '2021-06-25 10:05:07', 0, 'ajantaapdrops2811'),
(44, 'vinay@gmail.com', 'vinaymaurya', NULL, '2021-06-25 10:04:12', '2021-06-25 10:04:12', '2021-06-25 10:47:17', 0, 'ajantaapdrops2811'),
(45, 'rakshith@neosoulsoft.com', 'rakshith@nesoulsoft.com', NULL, '2021-06-25 11:45:25', '2021-06-25 11:46:36', '2021-06-25 11:47:06', 0, 'ajantaapdrops2811'),
(46, 'bbb@gmail.com', 'bbb', NULL, '2021-06-25 11:46:56', '2021-06-25 11:46:56', '2021-06-25 11:47:26', 0, 'ajantaapdrops2811'),
(47, 'tanvi@neosoulsoft.com', 'rakshith@neosoulsoft.com', NULL, '2021-06-25 11:48:07', '2021-06-25 11:48:07', '2021-06-25 11:54:39', 0, 'ajantaapdrops2811'),
(48, 'rrr@gmail.com', 'man', NULL, '2021-06-25 11:49:25', '2021-06-25 11:49:25', '2021-06-25 11:52:26', 0, 'ajantaapdrops2811'),
(49, 'neeraj@coact.co.in', 'neeraj', NULL, '2021-06-25 11:52:52', '2022-01-20 11:38:38', '2022-01-20 13:04:32', 1, 'ajantaapdrops2811'),
(50, 'rakeshshetty40@gmail.com', 'rakshith', NULL, '2021-06-25 11:53:38', '2021-06-25 13:09:25', '2021-06-25 13:09:55', 0, 'ajantaapdrops2811'),
(51, 'pawan@coact.co.in', 'neeraj', NULL, '2021-06-25 11:54:25', '2021-06-25 11:54:25', '2021-06-25 11:59:56', 0, 'ajantaapdrops2811'),
(52, 'tanvi@neosoulsoft.com', 'mahesh', NULL, '2021-06-25 11:56:49', '2021-06-25 11:57:14', '2021-06-25 13:42:59', 0, 'ajantaapdrops2811'),
(53, 'sid@gmail.com', 'siddheshk', NULL, '2021-06-25 11:58:25', '2021-06-25 11:58:25', '2021-06-25 12:09:25', 0, 'ajantaapdrops2811'),
(54, 'sandeep@gmail.com', 'sandeep', NULL, '2021-06-25 12:09:13', '2021-06-25 12:09:13', '2021-06-25 12:15:13', 0, 'ajantaapdrops2811'),
(55, 'demo@gmail.com', 'demo', NULL, '2021-06-25 12:11:37', '2021-06-25 12:19:09', '2021-06-25 12:19:39', 0, 'ajantaapdrops2811'),
(56, 'dsgfdfhg@gmail.com', 'amazing escape', NULL, '2021-06-25 12:12:35', '2021-06-25 12:12:35', '2021-06-25 12:13:35', 0, 'ajantaapdrops2811'),
(57, 'kp@gmail.com', 'kp', NULL, '2021-06-25 12:13:05', '2021-06-25 12:13:05', '2021-06-25 12:14:08', 0, 'ajantaapdrops2811'),
(58, 'aj@gmail.com', 'pawan', NULL, '2021-06-25 12:13:16', '2021-06-25 12:13:16', '2021-06-25 17:11:33', 0, 'ajantaapdrops2811'),
(59, 'kp123@gmail.com', 'kp123', NULL, '2021-06-25 12:14:19', '2021-06-25 12:14:19', '2021-06-25 12:15:19', 0, 'ajantaapdrops2811'),
(60, 'reddy@gamil.com', 'neerajreddy', NULL, '2021-06-25 12:14:21', '2021-06-25 12:14:21', '2021-06-25 13:47:12', 0, 'ajantaapdrops2811'),
(61, 'sfskdm@gmail.com', 'sklanfdfjmdklfksdk', NULL, '2021-06-25 12:14:58', '2021-06-25 12:14:58', '2021-06-25 12:15:28', 0, 'ajantaapdrops2811'),
(62, 'giftsbyjd@gmail.com', 'demo', NULL, '2021-06-25 12:15:52', '2021-06-25 12:20:54', '2021-06-25 13:00:35', 0, 'ajantaapdrops2811'),
(63, 'kamlesh@mail.com', 'kamlesh', NULL, '2021-06-28 19:31:39', '2021-06-28 19:31:39', '2021-06-28 20:00:40', 0, 'ajantaapdrops2811'),
(64, 'powashley74@gmail.com', 'powashley', NULL, '2021-06-29 13:32:59', '2021-06-30 13:28:10', '2021-06-30 13:29:40', 0, 'ajantaapdrops2811'),
(65, 'sandypagaria@gmail.com', 'demo', NULL, '2021-06-30 13:11:36', '2021-07-02 12:02:38', '2021-07-02 13:38:29', 1, 'ajantaapdrops2811'),
(66, 'JAIDEEP.PAGARIA@GMAIL.COM', 'xzfcfbh', NULL, '2021-06-30 13:14:14', '2021-06-30 13:14:14', '2021-06-30 13:14:44', 0, 'ajantaapdrops2811'),
(67, 'jaideep.pagaria@gmail.com', 'vhgmnhmhn', NULL, '2021-06-30 13:15:14', '2021-06-30 13:16:05', '2021-06-30 13:16:35', 0, 'ajantaapdrops2811'),
(68, 'demo@demo.com', 'demo', NULL, '2021-06-30 13:16:27', '2021-06-30 16:52:02', '2021-06-30 17:34:50', 0, 'ajantaapdrops2811'),
(69, 'xyz@mail.com', 'xyz', NULL, '2021-06-30 13:25:45', '2021-06-30 13:29:44', '2021-06-30 13:30:45', 0, 'ajantaapdrops2811'),
(70, 'tae@gmail.com', 'tae', NULL, '2021-06-30 13:30:12', '2021-06-30 13:30:42', '2021-06-30 13:57:12', 0, 'ajantaapdrops2811'),
(71, 'abc@mail.com', 'abc', NULL, '2021-06-30 13:30:12', '2021-06-30 13:31:04', '2021-06-30 13:44:05', 0, 'ajantaapdrops2811'),
(72, 'neha@gmail.com', 'neha', NULL, '2021-06-30 13:57:43', '2021-06-30 13:58:11', '2021-06-30 14:21:12', 0, 'ajantaapdrops2811'),
(73, 'pandu@gamil.com', 'pandu', NULL, '2021-06-30 17:03:58', '2021-06-30 17:03:58', '2021-06-30 23:24:00', 0, 'ajantaapdrops2811'),
(74, 'revanthreddy@gmail.com', 'revanth', NULL, '2021-07-01 11:34:51', '2021-07-01 11:34:51', '2021-07-01 14:49:22', 1, 'ajantaapdrops2811'),
(75, 'infosecdevelopers@gmail.com', 'ashley', NULL, '2021-07-03 19:39:17', '2021-07-03 19:39:17', '2021-07-03 23:09:54', 1, 'ajantaapdrops2811'),
(76, 'viraj@coact.co.in', 'OPS_Team', NULL, '2021-07-26 20:05:17', '2022-01-07 17:25:29', '2022-01-07 17:53:01', 1, 'ajantaapdrops2811'),
(77, 'akshat@coact.co.in', 'Akshat', NULL, '2022-01-19 14:18:06', '2022-01-19 14:18:06', '2022-01-19 14:29:07', 1, 'ajantaapdrops2811');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `master_leaderboard`
--
ALTER TABLE `master_leaderboard`
  ADD PRIMARY KEY (`SESSION_ID`);

--
-- Indexes for table `scores`
--
ALTER TABLE `scores`
  ADD PRIMARY KEY (`SCORE_ID`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `scores`
--
ALTER TABLE `scores`
  MODIFY `SCORE_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
