<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Sinamics</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<body>
<div class="container-fluid mt-2 ">
        <div class="row">
        <div class="col-12 m-0 col-md-6">
        <section>
<form  id="login-form" method="post" role="form" class = "log-form">
<hgroup>
   
 <p>Please enter your details to sign into your account</p>
</hgroup>
    <div class="group log-input">
    <div id="login-message"></div>
        
        <input type="text" id = "username"  placeholder="Enter Your username" aria-label="Email" aria-describedby="basic-addon1" name="username" id="email" required>
       
    
    </div>
    <div class="group log-input">
    <div id="login-message"></div>
        
              <input type="email" id = "username"  placeholder="Enter Your email Id" aria-label="Email" aria-describedby="basic-addon1" name="email" id="email" required>
  
    
    </div>
    <!-- <input type="text" name="username" id="username"> -->
  <div class="container-log-btn">
        <button  id="login" type="submit" name = "btn_submit" class="log-form-btn">
          <span>Login</span>
        </button>
      </div>

</form>
</section>

        </div>
        <div class="col-12 col-md-6 ">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators ">
    <li data-target="#carouselExampleIndicators" data-slide-to="0"  class="active bg-dark"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1" class="bg-dark"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2" class="bg-dark"></li>
  </ol>
  <div class="carousel-inner"  >
    <div class="carousel-item active">
      <img class="d-block " src="img/accountPage.png" width="100% "   alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block " src="img/3.jpg" width="100% "  alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block " src="img/2.jpg"  width="100% "  alt="Third slide">
    </div>
  </div>


<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).on('submit', '#login-form', function()
{  
  $.post('chkforlogin.php', $(this).serialize(), function(data)
  {
     // console.log(data);
      if(data == 's')
      {
        window.location.href='webcast.php';  
      }
      else if (data == '-1')
      {
          $('#login-message').text('You are already logged in. Please logout and try again.');
          $('#login-message').addClass('alert-danger').fadeIn().delay(3000).fadeOut();
          return false;
      }
      else
      {
          $('#login-message').text(data);
          $('#login-message').addClass('alert-danger').fadeIn().delay(3000).fadeOut();
          return false;
      }
  });
  
  return false;
});
$('.carousel').carousel({
  interval: 1000
})
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-13');
</script>

</body>
</html>