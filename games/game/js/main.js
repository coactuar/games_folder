function CButtonGrid(a, e, c, d, f) {
  var b, h, g, l, k;
  this._init = function (a, c, f, d, e) {
    b = !0;
    h = a;
    g = c;
    l = "state_0";
    k = createSprite(e, "state_0", 0, 0, CELL_WIDTH, CELL_HEIGHT);
    k.stop();
    k.setTransform(f, d, GRID_SCALE, GRID_SCALE);
    s_oStage.addChild(k);
    k.on("pressup", this.cellRelease);
  };
  this.unload = function () {
    b = !1;
    k.off("pressup", this.cellRelease);
    s_oStage.removeChild(k);
  };
  this.remove = function () {
    this.unload();
  };
  this.setState = function (a) {
    k.gotoAndStop("state_" + a);
    l = "state_" + a;
  };
  this.cellRelease = function (a) {
    !1 === s_bMobile && 2 == a.nativeEvent.button
      ? s_oGame.cellRightClick(h, g)
      : s_oGame.cellClicked(h, g);
  };
  this.getState = function () {
    return l;
  };
  this.isActive = function () {
    return b;
  };
  this._init(a, e, c, d, f);
}
function CSpriteLibrary() {
  var a, e, c, d, f, b;
  this.init = function (h, g, l) {
    c = e = 0;
    d = h;
    f = g;
    b = l;
    a = {};
  };
  this.addSprite = function (b, g) {
    a.hasOwnProperty(b) || ((a[b] = { szPath: g, oSprite: new Image() }), e++);
  };
  this.getSprite = function (b) {
    return a.hasOwnProperty(b) ? a[b].oSprite : null;
  };
  this._onSpritesLoaded = function () {
    f.call(b);
  };
  this._onSpriteLoaded = function () {
    d.call(b);
    ++c == e && this._onSpritesLoaded();
  };
  this.loadSprites = function () {
    for (var b in a)
      (a[b].oSprite.oSpriteLibrary = this),
        (a[b].oSprite.onload = function () {
          this.oSpriteLibrary._onSpriteLoaded();
        }),
        (a[b].oSprite.src = a[b].szPath);
  };
  this.getNumSprites = function () {
    return e;
  };
}
var CANVAS_WIDTH = 870,
  CANVAS_HEIGHT = 1504,
  EDGEBOARD_X = 50,
  EDGEBOARD_Y = 240,
  FONT_GAME = "Arial",
  FPS_TIME = 1e3 / 24,
  DISABLE_SOUND_MOBILE = !1,
  STATE_LOADING = 0,
  STATE_MENU = 1,
  STATE_HELP = 1,
  STATE_GAME = 3,
  ON_MOUSE_DOWN = 0,
  ON_MOUSE_UP = 1,
  ON_MOUSE_OVER = 2,
  ON_MOUSE_OUT = 3,
  ON_DRAG_START = 4,
  ON_DRAG_END = 5,
  BOARD_COLS,
  BOARD_ROWS,
  CELL_WIDTH = 60,
  CELL_HEIGHT = 60,
  GRID_SCALE,
  NUM_MINES,
  SCORE_FOR_ELEM = 50,
  TIME_LEVEL,
  BOARD_OFFSET_X = 40,
  BOARD_OFFSET_Y = 360,
  DIFF_EASY,
  DIFF_MEDIUM,
  DIFF_HARD;
function CToggle(a, e, c, d) {
  var f, b, h, g;
  this._init = function (a, c, d, e) {
    b = [];
    h = [];
    var p = new createjs.SpriteSheet({
      images: [d],
      frames: {
        width: d.width / 2,
        height: d.height,
        regX: d.width / 2 / 2,
        regY: d.height / 2,
      },
      animations: { state_false: [0, 1], state_true: [1, 2] },
    });
    f = e;
    g = createSprite(
      p,
      "state_" + f,
      d.width / 2 / 2,
      d.height / 2,
      d.width / 2,
      d.height
    );
    g.x = a;
    g.y = c;
    g.stop();
    s_oStage.addChild(g);
    this._initListener();
  };
  this.unload = function () {
    g.off("mousedown", this.buttonDown);
    g.off("pressup", this.buttonRelease);
    s_oStage.removeChild(g);
  };
  this.setPosition = function (a, b) {
    g.x = a;
    g.y = b;
  };
  this._initListener = function () {
    g.on("mousedown", this.buttonDown);
    g.on("pressup", this.buttonRelease);
  };
  this.addEventListener = function (a, c, g) {
    b[a] = c;
    h[a] = g;
  };
  this.buttonRelease = function () {
    g.scaleX = 1;
    g.scaleY = 1;
    f = !f;
    g.gotoAndStop("state_" + f);
    b[ON_MOUSE_UP] && b[ON_MOUSE_UP].call(h[ON_MOUSE_UP], f);
  };
  this.buttonDown = function () {
    g.scaleX = 0.9;
    g.scaleY = 0.9;
    b[ON_MOUSE_DOWN] && b[ON_MOUSE_DOWN].call(h[ON_MOUSE_DOWN]);
  };
  this._init(a, e, c, d);
}
var s_iOffsetX, s_iOffsetY;
(function (a) {
  (jQuery.browser = jQuery.browser || {}).mobile =
    /android|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(ad|hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|tablet|treo|up\.(browser|link)|vodafone|wap|webos|windows (ce|phone)|xda|xiino/i.test(
      a
    ) ||
    /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-/i.test(
      a.substr(0, 4)
    );
})(navigator.userAgent || navigator.vendor || window.opera);
$(window).resize(function () {
  sizeHandler();
});
function trace(a) {
  console.log(a);
}
function isIphone() {
  return -1 !== navigator.userAgent.toLowerCase().indexOf("iphone") ? !0 : !1;
}
function getSize(a) {
  var e = a.toLowerCase(),
    c = window.document,
    d = c.documentElement;
  if (void 0 === window["inner" + a]) a = d["client" + a];
  else if (window["inner" + a] != d["client" + a]) {
    var f = c.createElement("body");
    f.id = "vpw-test-b";
    f.style.cssText = "overflow:scroll";
    var b = c.createElement("div");
    b.id = "vpw-test-d";
    b.style.cssText = "position:absolute;top:-1000px";
    b.innerHTML =
      "<style>@media(" +
      e +
      ":" +
      d["client" + a] +
      "px){body#vpw-test-b div#vpw-test-d{" +
      e +
      ":7px!important}}</style>";
    f.appendChild(b);
    d.insertBefore(f, c.head);
    a = 7 == b["offset" + a] ? d["client" + a] : window["inner" + a];
    d.removeChild(f);
  } else a = window["inner" + a];
  return a;
}
window.addEventListener("orientationchange", onOrientationChange);
function onOrientationChange() {
  window.matchMedia("(orientation: portrait)").matches && sizeHandler();
  window.matchMedia("(orientation: landscape)").matches && sizeHandler();
}
function getIOSWindowHeight() {
  return (
    (document.documentElement.clientWidth / window.innerWidth) *
    window.innerHeight
  );
}
function getHeightOfIOSToolbars() {
  var a =
    (0 === window.orientation ? screen.height : screen.width) -
    getIOSWindowHeight();
  return 1 < a ? a : 0;
}
function sizeHandler() {
  window.scrollTo(0, 1);
  if ($("#canvas")) {
    var a;
    a = navigator.userAgent.match(/(iPad|iPhone|iPod)/g)
      ? getIOSWindowHeight()
      : getSize("Height");
    var e = getSize("Width"),
      c = Math.min(a / CANVAS_HEIGHT, e / CANVAS_WIDTH),
      d = CANVAS_WIDTH * c,
      c = CANVAS_HEIGHT * c,
      f = 0;
    c < a
      ? ((f = a - c), (c += f), (d += (CANVAS_WIDTH / CANVAS_HEIGHT) * f))
      : d < e &&
        ((f = e - d), (d += f), (c += (CANVAS_HEIGHT / CANVAS_WIDTH) * f));
    var f = a / 2 - c / 2,
      b = e / 2 - d / 2,
      h = CANVAS_WIDTH / d;
    if (b * h < -EDGEBOARD_X || f * h < -EDGEBOARD_Y)
      (c = Math.min(
        a / (CANVAS_HEIGHT - 2 * EDGEBOARD_Y),
        e / (CANVAS_WIDTH - 2 * EDGEBOARD_X)
      )),
        (d = CANVAS_WIDTH * c),
        (c *= CANVAS_HEIGHT),
        (f = (a - c) / 2),
        (b = (e - d) / 2),
        (h = CANVAS_WIDTH / d);
    s_iOffsetX = -1 * b * h;
    s_iOffsetY = -1 * f * h;
    0 <= f && (s_iOffsetY = 0);
    0 <= b && (s_iOffsetX = 0);
    null !== s_oInterface &&
      s_oInterface.refreshButtonPos(s_iOffsetX, s_iOffsetY);
    null !== s_oMenu && s_oMenu.refreshButtonPos(s_iOffsetX, s_iOffsetY);
    $("#canvas").css("width", d + "px");
    $("#canvas").css("height", c + "px");
    0 > f ? $("#canvas").css("top", f + "px") : $("#canvas").css("top", "0px");
    $("#canvas").css("left", b + "px");
  }
}
function createBitmap(a, e, c) {
  var d = new createjs.Bitmap(a),
    f = new createjs.Shape();
  e && c
    ? f.graphics.beginFill("#fff").drawRect(0, 0, e, c)
    : f.graphics.beginFill("#ff0").drawRect(0, 0, a.width, a.height);
  d.hitArea = f;
  return d;
}
function createSprite(a, e, c, d, f, b) {
  a = null !== e ? new createjs.Sprite(a, e) : new createjs.Sprite(a);
  e = new createjs.Shape();
  e.graphics.beginFill("#000000").drawRect(-c, -d, f, b);
  a.hitArea = e;
  return a;
}
function getMobileOperatingSystem() {
  var a = navigator.userAgent || navigator.vendor || window.opera;
  return a.match(/iPad/i) || a.match(/iPhone/i) || a.match(/iPod/i)
    ? "ios"
    : a.match(/Android/i)
    ? "android"
    : "unknown";
}
function inIframe() {
  try {
    return window.self !== window.top;
  } catch (a) {
    return !0;
  }
}
function randomFloatBetween(a, e, c) {
  "undefined" === typeof c && (c = 2);
  return parseFloat(Math.min(a + Math.random() * (e - a), e).toFixed(c));
}
function shuffle(a) {
  for (var e = a.length, c, d; 0 !== e; )
    (d = Math.floor(Math.random() * e)),
      --e,
      (c = a[e]),
      (a[e] = a[d]),
      (a[d] = c);
  return a;
}
function formatTime(a) {
  a /= 1e3;
  var e = Math.floor(a / 60);
  a = parseFloat(a - 60 * e).toFixed(1);
  var c = "",
    c = 10 > e ? c + ("0" + e + ":") : c + (e + ":");
  return (c = 10 > a ? c + ("0" + a) : c + a);
}
function NoClickDelay(a) {
  this.element = a;
  window.Touch && this.element.addEventListener("touchstart", this, !1);
}
NoClickDelay.prototype = {
  handleEvent: function (a) {
    switch (a.type) {
      case "touchstart":
        this.onTouchStart(a);
        break;
      case "touchmove":
        this.onTouchMove(a);
        break;
      case "touchend":
        this.onTouchEnd(a);
    }
  },
  onTouchStart: function (a) {
    a.preventDefault();
    this.moved = !1;
    this.element.addEventListener("touchmove", this, !1);
    this.element.addEventListener("touchend", this, !1);
  },
  onTouchMove: function (a) {
    this.moved = !0;
  },
  onTouchEnd: function (a) {
    this.element.removeEventListener("touchmove", this, !1);
    this.element.removeEventListener("touchend", this, !1);
    if (!this.moved) {
      a = document.elementFromPoint(
        a.changedTouches[0].clientX,
        a.changedTouches[0].clientY
      );
      3 == a.nodeType && (a = a.parentNode);
      var e = document.createEvent("MouseEvents");
      e.initEvent("click", !0, !0);
      a.dispatchEvent(e);
    }
  },
};
function ctlArcadeResume() {
  null !== s_oMain && s_oMain.startUpdate();
}
function ctlArcadePause() {
  null !== s_oMain && s_oMain.stopUpdate();
}
function CTextButton(a, e, c, d, f, b, h) {
  var g, l, k;
  this._init = function (a, b, c, d, f, h, e) {
    g = [];
    l = [];
    var y = createBitmap(c),
      x = Math.ceil(e / 20),
      v = new createjs.Text(d, "bold " + e + "px " + f, "#000000");
    v.textAlign = "center";
    var w = v.getBounds();
    v.x = c.width / 2 + x;
    v.y = (c.height - w.height) / 2 + x;
    d = new createjs.Text(d, "bold " + e + "px " + f, h);
    d.textAlign = "center";
    w = d.getBounds();
    d.x = c.width / 2;
    d.y = (c.height - w.height) / 2;
    k = new createjs.Container();
    k.x = a;
    k.y = b;
    k.regX = c.width / 2;
    k.regY = c.height / 2;
    k.addChild(y, v, d);
    s_oStage.addChild(k);
    this._initListener();
  };
  this.unload = function () {
    k.off("mousedown");
    k.off("pressup");
    s_oStage.removeChild(k);
  };
  this.setPosition = function (a, b) {
    k.x = a;
    k.y = b;
  };
  this.setVisible = function (a) {
    k.visible = a;
  };
  this._initListener = function () {
    oParent = this;
    k.on("mousedown", this.buttonDown);
    k.on("pressup", this.buttonRelease);
  };
  this.addEventListener = function (a, b, c) {
    g[a] = b;
    l[a] = c;
  };
  this.buttonRelease = function () {
    k.scaleX = 1;
    k.scaleY = 1;
    g[ON_MOUSE_UP] && g[ON_MOUSE_UP].call(l[ON_MOUSE_UP]);
  };
  this.buttonDown = function () {
    k.scaleX = 0.9;
    k.scaleY = 0.9;
    g[ON_MOUSE_DOWN] && g[ON_MOUSE_DOWN].call(l[ON_MOUSE_DOWN]);
  };
  this.setPosition = function (a, b) {
    k.x = a;
    k.y = b;
  };
  this.setX = function (a) {
    k.x = a;
  };
  this.setY = function (a) {
    k.y = a;
  };
  this.getButtonImage = function () {
    return k;
  };
  this.getX = function () {
    return k.x;
  };
  this.getY = function () {
    return k.y;
  };
  this._init(a, e, c, d, f, b, h);
  return this;
}
function CPreloader() {
  var a, e, c, d, f, b, h;
  this._init = function () {
    s_oSpriteLibrary.init(this._onImagesLoaded, this._onAllImagesLoaded, this);
    s_oSpriteLibrary.addSprite("bg_menu", "./sprites/bg_menu.jpg");
    s_oSpriteLibrary.addSprite("progress_bar", "./sprites/progress_bar.png");
    s_oSpriteLibrary.loadSprites();
    h = new createjs.Container();
    s_oStage.addChild(h);
  };
  this.unload = function () {
    h.removeAllChildren();
  };
  this._onImagesLoaded = function () {};
  this._onAllImagesLoaded = function () {
    this.attachSprites();
    s_oMain.preloaderReady();
  };
  this.attachSprites = function () {
    var g = createBitmap(s_oSpriteLibrary.getSprite("bg_menu"));
    h.addChild(g);
    g = s_oSpriteLibrary.getSprite("progress_bar");
    d = createBitmap(g);
    d.x = CANVAS_WIDTH / 2 - g.width / 2;
    d.y = CANVAS_HEIGHT - 370;
    h.addChild(d);
    a = g.width;
    e = g.height;
    f = new createjs.Shape();
    f.graphics.beginFill("rgba(255,255,255,0.01)").drawRect(d.x, d.y, 1, e);
    h.addChild(f);
    d.mask = f;
    c = new createjs.Text("", "30px " + FONT_GAME, "#fff");
    c.x = CANVAS_WIDTH / 2;
    c.y = CANVAS_HEIGHT - 325;
    c.shadow = new createjs.Shadow("#000", 2, 2, 2);
    c.textBaseline = "alphabetic";
    c.textAlign = "center";
    h.addChild(c);
    b = new createjs.Shape();
    b.graphics.beginFill("black").drawRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
    b.alpha = 0;
    h.addChild(b);
  };
  this.refreshLoader = function (b) {
    c.text = b + "%";
    f.graphics.clear();
    b = Math.floor((b * a) / 100);
    f.graphics.beginFill("rgba(255,255,255,0.01)").drawRect(d.x, d.y, b, e);
  };
  this._init();
}
function CMenu() {
  var a, e, c, d, f, b, h, g;
  this._init = function () {
    f = createBitmap(s_oSpriteLibrary.getSprite("bg_menu"));
    s_oStage.addChild(f);
    var l = s_oSpriteLibrary.getSprite("but_play");
    a = CANVAS_WIDTH / 2;
    e = CANVAS_HEIGHT - 150;
    b = new CTextButton(a, e, l, TEXT_PLAY, FONT_GAME, "#ffffff", 50);
    b.addEventListener(ON_MOUSE_UP, this._onButPlayRelease, this);
    if (!1 === DISABLE_SOUND_MOBILE || !1 === s_bMobile)
      (l = s_oSpriteLibrary.getSprite("audio_icon")),
        (c = CANVAS_WIDTH - 60),
        (d = 60),
        (h = new CToggle(c, d, l, s_bAudioActive)),
        h.addEventListener(ON_MOUSE_UP, this._onAudioToggle, this);
    g = new createjs.Shape();
    g.graphics.beginFill("black").drawRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
    s_oStage.addChild(g);
    createjs.Tween.get(g)
      .to({ alpha: 0 }, 400)
      .call(function () {
        g.visible = !1;
      });
    this.refreshButtonPos(s_iOffsetX, s_iOffsetY);
  };
  this.refreshButtonPos = function (g, f) {
    (!1 !== DISABLE_SOUND_MOBILE && !1 !== s_bMobile) ||
      h.setPosition(c - g, f + d);
    b.setPosition(a, e - f);
  };
  this.unload = function () {
    b.unload();
    b = null;
    !1 === s_bMobile && (h.unload(), (h = null));
    s_oStage.removeAllChildren();
    s_oMenu = null;
  };
  this._onButPlayRelease = function () {
    this.unload();
    $(s_oMain).trigger("start_session");
    s_oMain.gotoGame();
  };
  this._onAudioToggle = function () {
    createjs.Sound.setMute(s_bAudioActive);
    s_bAudioActive = !s_bAudioActive;
  };
  s_oMenu = this;
  this._init();
}
var s_oMenu = null;
function CMain(a) {
  var e,
    c = 0,
    d = 0,
    f = STATE_LOADING,
    b,
    h,
    g;
  this.initContainer = function () {
    var a = document.getElementById("canvas");
    s_oStage = new createjs.Stage(a);
    createjs.Touch.enable(s_oStage);
    s_bMobile = jQuery.browser.mobile;
    !1 === s_bMobile &&
      (s_oStage.enableMouseOver(20),
      $("body").on("contextmenu", "#canvas", function (a) {
        return !1;
      }));
    s_iPrevTime = new Date().getTime();
    createjs.Ticker.addEventListener("tick", this._update);
    s_oSpriteLibrary = new CSpriteLibrary();
    navigator.userAgent.match(/Windows Phone/i) && (DISABLE_SOUND_MOBILE = !0);
    h = new CPreloader();
  };
  this.preloaderReady = function () {
    (!1 !== DISABLE_SOUND_MOBILE && !1 !== s_bMobile) || this._initSounds();
    this._loadImages();
    e = !0;
  };
  this.soundLoaded = function () {
    c++;
    h.refreshLoader(Math.floor((c / d) * 100));
    if (c === d) {
      h.unload();
      if (!1 === DISABLE_SOUND_MOBILE || !1 === s_bMobile)
        s_oSoundTrackSnd = createjs.Sound.play("soundtrack", {
          interrupt: createjs.Sound.INTERRUPT_ANY,
          loop: -1,
          volume: 0.5,
        });
      this.gotoMenu();
    }
  };
  this._initSounds = function () {
    createjs.Sound.initializeDefaultPlugins() &&
      ((createjs.Sound.alternateExtensions = ["ogg"]),
      createjs.Sound.addEventListener(
        "fileload",
        createjs.proxy(this.soundLoaded, this)
      ),
      createjs.Sound.registerSound("./sounds/soundtrack.mp3", "soundtrack"),
      createjs.Sound.registerSound("./sounds/game_over.mp3", "game_over"),
      createjs.Sound.registerSound("./sounds/rollover.mp3", "rollover"),
      (d += 3));
  };
  this._loadImages = function () {
    s_oSpriteLibrary.init(this._onImagesLoaded, this._onAllImagesLoaded, this);
    s_oSpriteLibrary.addSprite("but_play", "./sprites/but_play.png");
    s_oSpriteLibrary.addSprite("but_exit", "./sprites/but_exit.png");
    s_oSpriteLibrary.addSprite("bg_menu", "./sprites/bg_menu.jpg");
    s_oSpriteLibrary.addSprite("bg_game", "./sprites/bg_game.jpg");
    s_oSpriteLibrary.addSprite("msg_box", "./sprites/msg_box.png");
    s_oSpriteLibrary.addSprite("bg_help", "./sprites/bg_help.png");
    s_oSpriteLibrary.addSprite("cell_bg", "./sprites/cell_bg.png");
    s_oSpriteLibrary.addSprite("button_grid", "./sprites/button_grid.png");
    s_oSpriteLibrary.addSprite("toggle_flag", "./sprites/toggle_flag.png");
    s_oSpriteLibrary.addSprite("mine", "./sprites/mine.png");
    s_oSpriteLibrary.addSprite("audio_icon", "./sprites/audio_icon.png");
    d += s_oSpriteLibrary.getNumSprites();
    s_oSpriteLibrary.loadSprites();
  };
  this._onImagesLoaded = function () {
    c++;
    h.refreshLoader(Math.floor((c / d) * 100));
    if (c === d) {
      h.unload();
      if (!1 === DISABLE_SOUND_MOBILE || !1 === s_bMobile)
        s_oSoundTrackSnd = createjs.Sound.play("soundtrack", {
          interrupt: createjs.Sound.INTERRUPT_ANY,
          loop: -1,
          volume: 0.5,
        });
      this.gotoMenu();
    }
  };
  this._onAllImagesLoaded = function () {};
  this.onAllPreloaderImagesLoaded = function () {
    this._loadImages();
  };
  this.gotoMenu = function () {
    new CMenu();
    f = STATE_MENU;
  };
  this.gotoGame = function () {
    g = new CGame(b);
    f = STATE_GAME;
  };
  this.gotoHelp = function () {
    new CHelp();
    f = STATE_HELP;
  };
  this.stopUpdate = function () {
    e = !1;
    createjs.Ticker.paused = !0;
    $("#block_game").css("display", "block");
  };
  this.startUpdate = function () {
    s_iPrevTime = new Date().getTime();
    e = !0;
    createjs.Ticker.paused = !1;
    $("#block_game").css("display", "none");
  };
  this._update = function (a) {
    if (!1 !== e) {
      var b = new Date().getTime();
      s_iTimeElaps = b - s_iPrevTime;
      s_iCntTime += s_iTimeElaps;
      s_iCntFps++;
      s_iPrevTime = b;
      1e3 <= s_iCntTime &&
        ((s_iCurFps = s_iCntFps), (s_iCntTime -= 1e3), (s_iCntFps = 0));
      f === STATE_GAME && g.update();
      s_oStage.update(a);
    }
  };
  s_oMain = this;
  b = a;
  this.initContainer();
}
var s_bMobile,
  s_bAudioActive = !0,
  s_iCntTime = 0,
  s_iTimeElaps = 0,
  s_iPrevTime = 0,
  s_iCntFps = 0,
  s_iCurFps = 0,
  s_oSoundTrackSnd,
  s_oDrawLayer,
  s_oStage,
  s_oMain = null,
  s_oSpriteLibrary;
TEXT_GAMEOVER = "GAME OVER";
TEXT_CONGRATS = "CONGRATULATIONS";
TEXT_SCORE = "SCORE";
TEXT_TIME = "TIME";
TEXT_PLAY = "PLAY";
TEXT_HELP1 = "DISCLOSE ALL GRID CELLS AND AVOID THE MINES";
TEXT_HELP2 = "MARK WITH A FLAG ALL GRID CELLS WHERE YOU THINK THERE'S A MINE ";
function CInterface() {
  var a, e, c, d, f, b, h, g, l, k, m, n, p, q, t, u;
  this._init = function () {
    var r = s_oSpriteLibrary.getSprite("but_exit");
    f = CANVAS_WIDTH - r.width / 2 - 10;
    b = 10 + r.height / 2;
    h = new CGfxButton(f, b, r, !0);
    h.addEventListener(ON_MOUSE_UP, this._onExit, this);
    k = new createjs.Text("", "bold 40px " + FONT_GAME, "#000000");
    k.x = CANVAS_WIDTH - 184;
    k.y = CANVAS_HEIGHT - 353;
    k.textAlign = "center";
    s_oStage.addChild(k);
    m = new createjs.Text("", "bold 40px " + FONT_GAME, "#ffffff");
    m.x = CANVAS_WIDTH - 182;
    m.y = CANVAS_HEIGHT - 355;
    m.textAlign = "center";
    s_oStage.addChild(m);
    t = new createjs.Text(
      TEXT_SCORE + ": 0",
      "bold 42px " + FONT_GAME,
      "#000000"
    );
    t.x = BOARD_OFFSET_X;
    t.y = BOARD_OFFSET_Y - 48;
    t.textAlign = "left";
    s_oStage.addChild(t);
    u = new createjs.Text(
      TEXT_SCORE + ": 0",
      "bold 42px " + FONT_GAME,
      "#ffffff"
    );
    u.x = BOARD_OFFSET_X;
    u.y = BOARD_OFFSET_Y - 50;
    u.textAlign = "left";
    s_oStage.addChild(u);
    if (!1 === DISABLE_SOUND_MOBILE || !1 === s_bMobile)
      (r = s_oSpriteLibrary.getSprite("audio_icon")),
        (c = CANVAS_WIDTH - 180),
        (d = 10 + r.height / 2),
        (g = new CToggle(c, d, r, s_bAudioActive)),
        g.addEventListener(ON_MOUSE_UP, this._onAudioToggle, this);
    s_bMobile &&
      ((t.x = CANVAS_WIDTH / 2 + 2),
      (u.x = CANVAS_WIDTH / 2),
      (t.textAlign = "center"),
      (u.textAlign = "center"),
      (a = 100),
      (e = 60),
      (l = new CToggle(a, e, s_oSpriteLibrary.getSprite("toggle_flag"), !1)),
      l.addEventListener(ON_MOUSE_UP, this._onToggleFlag, this));
    r = s_oSpriteLibrary.getSprite("mine");
    q = createBitmap(r);
    q.x = BOARD_OFFSET_X;
    q.y = CANVAS_HEIGHT - 366;
    s_oStage.addChild(q);
    n = new createjs.Text(NUM_MINES, "bold 40px " + FONT_GAME, "#000000");
    n.x = q.x + r.width + 2;
    n.y = CANVAS_HEIGHT - 353;
    n.textAlign = "left";
    s_oStage.addChild(n);
    p = new createjs.Text(NUM_MINES, "bold 40px " + FONT_GAME, "#ffffff");
    p.x = q.x + r.width;
    p.y = CANVAS_HEIGHT - 355;
    p.textAlign = "left";
    s_oStage.addChild(p);
    this.refreshButtonPos(s_iOffsetX, s_iOffsetY);
  };
  this.unload = function () {
    h.unload();
    h = null;
    !1 === s_bMobile && (g.unload(), (g = null));
    s_oStage.removeChild(m);
    s_oStage.removeChild(k);
    s_oStage.removeChild(n);
    s_oStage.removeChild(p);
    s_oInterface = null;
  };
  this.refreshButtonPos = function (p, q) {
    (!1 !== DISABLE_SOUND_MOBILE && !1 !== s_bMobile) ||
      g.setPosition(c - p, q + d);
    s_bMobile && l.setPosition(a + p, q + e);
    h.setPosition(f - p, b + q);
  };
  this.refreshTime = function (a) {
    a = formatTime(a);
    m.text = TEXT_TIME + ": " + a;
    k.text = TEXT_TIME + ": " + a;
  };
  this.refreshMines = function (a) {
    n.text = "" + a;
    p.text = "" + a;
  };
  this.refreshScore = function (a) {
    t.text = TEXT_SCORE + ": " + a;
    u.text = TEXT_SCORE + ": " + a;
  };
  this._onExit = function () {
    s_oGame.onExit();
  };
  this._onToggleFlag = function (a) {
    s_oGame.setFlagActive(a);
  };
  this._onAudioToggle = function () {
    createjs.Sound.setMute(s_bAudioActive);
    s_bAudioActive = !s_bAudioActive;
  };
  s_oInterface = this;
  this._init();
  return this;
}
var s_oInterface = null;
function CHelpPanel(a) {
  var e, c, d, f;
  this._init = function (a) {
    d = createBitmap(a);
    c = new createjs.Text(TEXT_HELP1, "bold 36px " + FONT_GAME, "#000000");
    c.textAlign = "center";
    c.x = CANVAS_WIDTH / 2 + 2;
    c.y = 672;
    c.lineWidth = CANVAS_WIDTH - 200;
    e = new createjs.Text(TEXT_HELP1, "bold 36px " + FONT_GAME, "#ffffff");
    e.textAlign = "center";
    e.x = CANVAS_WIDTH / 2;
    e.y = 670;
    e.lineWidth = CANVAS_WIDTH - 200;
    a = new createjs.Text(TEXT_HELP2, "bold 30px " + FONT_GAME, "#000000");
    a.textAlign = "center";
    a.x = CANVAS_WIDTH / 2 + 2;
    a.y = 849;
    a.lineWidth = CANVAS_WIDTH - 200;
    var h = new createjs.Text(TEXT_HELP2, "bold 30px " + FONT_GAME, "#ffffff");
    h.textAlign = "center";
    h.x = CANVAS_WIDTH / 2;
    h.y = 847;
    h.lineWidth = CANVAS_WIDTH - 200;
    f = new createjs.Container();
    f.addChild(d, c, e, a, h);
    s_oStage.addChild(f);
    var g = this;
    f.on("pressup", function () {
      g._onExitHelp();
    });
  };
  this.unload = function () {
    s_oStage.removeChild(f);
    var a = this;
    f.off("pressup", function () {
      a._onExitHelp();
    });
  };
  this._onExitHelp = function () {
    this.unload();
    s_oGame._onExitHelp();
  };
  this._init(a);
}
function CGfxButton(a, e, c) {
  var d, f, b;
  this._init = function (a, c, e) {
    d = [];
    f = [];
    b = createBitmap(e);
    b.x = a;
    b.y = c;
    b.regX = e.width / 2;
    b.regY = e.height / 2;
    s_oStage.addChild(b);
    this._initListener();
  };
  this.unload = function () {
    b.off("mousedown", this.buttonDown);
    b.off("pressup", this.buttonRelease);
    s_oStage.removeChild(b);
  };
  this.setVisible = function (a) {
    b.visible = a;
  };
  this.setPosition = function (a, c) {
    b.x = a;
    b.y = c;
  };
  this._initListener = function () {
    b.on("mousedown", this.buttonDown);
    b.on("pressup", this.buttonRelease);
  };
  this.addEventListener = function (a, b, c) {
    d[a] = b;
    f[a] = c;
  };
  this.buttonRelease = function () {
    b.scaleX = 1;
    b.scaleY = 1;
    d[ON_MOUSE_UP] && d[ON_MOUSE_UP].call(f[ON_MOUSE_UP]);
  };
  this.buttonDown = function () {
    b.scaleX = 0.9;
    b.scaleY = 0.9;
    d[ON_MOUSE_DOWN] && d[ON_MOUSE_DOWN].call(f[ON_MOUSE_DOWN]);
  };
  this.setPosition = function (a, c) {
    b.x = a;
    b.y = c;
  };
  this.setX = function (a) {
    b.x = a;
  };
  this.setY = function (a) {
    b.y = a;
  };
  this.getButtonImage = function () {
    return b;
  };
  this.getX = function () {
    return b.x;
  };
  this.getY = function () {
    return b.y;
  };
  this._init(a, e, c);
  return this;
}
function CGame(a) {
  var e = !1,
    c,
    d = !0,
    f,
    b,
    h,
    g,
    l,
    k,
    m,
    n;
  this._init = function () {
    c = !1;
    f = NUM_MINES;
    h = TIME_LEVEL;
    b = 0;
    k = createBitmap(s_oSpriteLibrary.getSprite("bg_game"));
    s_oStage.addChild(k);
    this._setGridSize();
    this.createGrid();
    this.assignGridCounter();
    this.createGridButton();
    m = new CInterface();
    new CHelpPanel(s_oSpriteLibrary.getSprite("bg_help"));
    $(s_oMain).trigger("start_level", 1);
  };
  this.unload = function () {
    m.unload();
    n && n.unload();
    for (var a = 0; a < BOARD_ROWS; a++)
      for (var b = 0; b < BOARD_COLS; b++) g[a][b].unload();
    s_oStage.removeAllChildren();
  };
  this._setGridSize = function () {
    var a = 1,
      b = 1,
      c = Math.floor((CANVAS_WIDTH - EDGEBOARD_X - 46) / BOARD_COLS);
    60 < c ? (GRID_SCALE = 1) : (a = c / CELL_WIDTH);
    c = Math.floor((CANVAS_HEIGHT - BOARD_OFFSET_Y) / BOARD_ROWS);
    60 < c ? (GRID_SCALE = 1) : (b = c / CELL_HEIGHT);
    GRID_SCALE = a < b ? a : b;
    BOARD_OFFSET_X = Math.floor(
      (CANVAS_WIDTH - CELL_WIDTH * GRID_SCALE * BOARD_COLS) / 2
    );
  };
  this.createGridButton = function () {
    var a = {
        images: [s_oSpriteLibrary.getSprite("button_grid")],
        frames: { width: CELL_WIDTH, height: CELL_HEIGHT, regX: 0, regY: 0 },
        animations: { state_0: [0, 1], state_1: [1, 2] },
      },
      a = new createjs.SpriteSheet(a),
      b = BOARD_OFFSET_X,
      c = BOARD_OFFSET_Y;
    l = [];
    for (var d = 0; d < BOARD_ROWS; d++) {
      l[d] = [];
      for (var f = 0; f < BOARD_COLS; f++)
        (l[d][f] = new CButtonGrid(d, f, b, c, a)),
          (b += CELL_WIDTH * GRID_SCALE);
      b = BOARD_OFFSET_X;
      c += CELL_HEIGHT * GRID_SCALE;
    }
  };
  this.createGrid = function () {
    var a = {
        images: [s_oSpriteLibrary.getSprite("cell_bg")],
        frames: { width: CELL_WIDTH, height: CELL_HEIGHT, regX: 0, regY: 0 },
        animations: {
          state_0: [0, 1],
          state_1: [1, 2],
          state_2: [2, 3],
          state_3: [3, 4],
          state_4: [4, 5],
          state_5: [5, 6],
          state_6: [6, 7],
          state_7: [7, 8],
          state_8: [8, 9],
          mine: [9, 10],
        },
      },
      a = new createjs.SpriteSheet(a),
      b = [];
    g = [];
    for (
      var c = BOARD_OFFSET_X, d = BOARD_OFFSET_Y, f = 0;
      f < BOARD_ROWS;
      f++
    ) {
      g[f] = [];
      for (var e = 0; e < BOARD_COLS; e++) {
        var h = new CCellGrid(c, d, a);
        g[f][e] = h;
        c += CELL_WIDTH * GRID_SCALE;
        b.push({ row: f, col: e });
      }
      c = BOARD_OFFSET_X;
      d += CELL_HEIGHT * GRID_SCALE;
    }
    this.putMines(b);
  };
  this.putMines = function (a) {
    for (var b = [], c = 0; c < NUM_MINES; c++) {
      var d = Math.floor(Math.random() * a.length);
      b.push(a[d]);
      a.splice(d, 1);
    }
    for (a = 0; a < b.length; a++) g[b[a].row][b[a].col].putMine();
  };
  this.resetGrid = function () {
    for (var a = [], b = 0; b < BOARD_ROWS; b++)
      for (var c = 0; c < BOARD_COLS; c++)
        g[b][c].reset(), a.push({ row: b, col: c });
    this.putMines(a);
    this.assignGridCounter();
  };
  this.assignGridCounter = function () {
    for (var a = 0; a < BOARD_ROWS; a++)
      for (var b = 0; b < BOARD_COLS; b++)
        if ("mine" !== g[a][b].getState()) {
          var c = this.checkAdjacent(a, b);
          g[a][b].changeState(c);
        }
  };
  this.checkAdjacent = function (a, b) {
    var c = 0;
    0 <= a - 1 && 0 <= b - 1 && "mine" === g[a - 1][b - 1].getState() && c++;
    0 <= a - 1 && "mine" === g[a - 1][b].getState() && c++;
    0 <= a - 1 &&
      b + 1 < BOARD_COLS &&
      "mine" === g[a - 1][b + 1].getState() &&
      c++;
    0 <= b - 1 && "mine" === g[a][b - 1].getState() && c++;
    b + 1 < BOARD_COLS && "mine" === g[a][b + 1].getState() && c++;
    a + 1 < BOARD_ROWS &&
      0 <= b - 1 &&
      "mine" === g[a + 1][b - 1].getState() &&
      c++;
    a + 1 < BOARD_ROWS && "mine" === g[a + 1][b].getState() && c++;
    a + 1 < BOARD_ROWS &&
      b + 1 < BOARD_COLS &&
      "mine" === g[a + 1][b + 1].getState() &&
      c++;
    return c;
  };
  this.cellRightClick = function (a, b) {
    c = !0;
    this.cellClicked(a, b);
    c = !1;
  };
  this.cellClicked = function (a, e) {
    if (c && "state_0" === l[a][e].getState())
      l[a][e].setState(1), f--, m.refreshMines(f);
    else if ("state_1" === l[a][e].getState())
      f++, m.refreshMines(f), l[a][e].setState(0);
    else {
      for (var h = g[a][e].getState(); d && "state_0" !== h; )
        this.resetGrid(), (h = g[a][e].getState());
      d = !1;
      l[a][e].remove();
      "mine" === h
        ? ((!1 !== DISABLE_SOUND_MOBILE && !1 !== s_bMobile) ||
            createjs.Sound.play("game_over"),
          this._gameOver())
        : ((!1 !== DISABLE_SOUND_MOBILE && !1 !== s_bMobile) ||
            createjs.Sound.play("rollover"),
          "state_0" === h && this.showAdjacentCell(a, e),
          this._checkWin() && this._win(),
          "undefined" !== typeof callGameAjax && callGameAjax(b));
    }
  };
  this.showAdjacentCell = function (a, b) {
    g[a][b].setChecked();
    for (var c = a - 1; c < a + 2; c++)
      for (var d = b - 1; d < b + 2; d++)
        0 <= c &&
          0 <= d &&
          c < BOARD_ROWS &&
          d < BOARD_COLS &&
          !1 === g[c][d].isChecked() &&
          "mine" !== g[c][d].getState() &&
          (l[c][d].remove(),
          "state_0" === g[c][d].getState() && this.showAdjacentCell(c, d));
  };
  this.setFlagActive = function (a) {
    c = a;
  };
  this._gameOver = function () {
    e = !1;
    n = CEndPanel(s_oSpriteLibrary.getSprite("msg_box"));
    n.show(b, !1);
  };
  this._win = function () {
    b += Math.floor(h / 100);
    m.refreshScore(b);
    e = !1;
    n = CEndPanel(s_oSpriteLibrary.getSprite("msg_box"));
    n.show(b, !0);
  };
  this._checkWin = function () {
    b = 0;
    for (
      var a = BOARD_ROWS * BOARD_ROWS - NUM_MINES, c = 0;
      c < BOARD_ROWS;
      c++
    )
      for (var d = 0; d < BOARD_COLS; d++)
        !1 === l[c][d].isActive() && (a--, (b += SCORE_FOR_ELEM));
    m.refreshScore(b);
    return 0 === a ? !0 : !1;
  };
  this.onExit = function () {
    this.unload();
    s_oMain.gotoMenu();
    $(s_oMain).trigger("end_session");
  };
  this._onExitHelp = function () {
    e = !0;
  };
  this.update = function () {
    !1 !== e &&
      ((h -= s_iTimeElaps),
      0 < h ? m.refreshTime(h) : ((h = 0), this._gameOver()));
  };
  BOARD_COLS = 30 < a.cols ? 30 : 10 > a.cols ? 10 : a.cols;
  BOARD_ROWS = 30 < a.rows ? 30 : 10 > a.rows ? 10 : a.rows;
  NUM_MINES =
    a.mines >= BOARD_ROWS * BOARD_COLS
      ? Math.floor((BOARD_ROWS * BOARD_COLS) / 2)
      : a.mines;
  TIME_LEVEL = a.time;
  s_oGame = this;
  this._init();
}
var s_oGame;
function CEndPanel(a) {
  var e, c, d, f, b, h;
  this._init = function (a) {
    e = createBitmap(a);
    b = new createjs.Text("", "bold 90px " + FONT_GAME, "#000");
    b.x = CANVAS_WIDTH / 2 + 2;
    b.y = CANVAS_HEIGHT / 2 - 100;
    b.textAlign = "center";
    f = new createjs.Text("", "bold 90px " + FONT_GAME, "#ffffff");
    f.x = CANVAS_WIDTH / 2;
    f.y = CANVAS_HEIGHT / 2 - 102;
    f.textAlign = "center";
    c = new createjs.Text("", "bold 52px " + FONT_GAME, "#000");
    c.x = CANVAS_WIDTH / 2 + 1;
    c.y = CANVAS_HEIGHT / 2 + 72;
    c.textAlign = "center";
    d = new createjs.Text("", "bold 52px " + FONT_GAME, "#ffffff");
    d.x = CANVAS_WIDTH / 2;
    d.y = CANVAS_HEIGHT / 2 + 70;
    d.textAlign = "center";
    h = new createjs.Container();
    h.alpha = 0;
    h.visible = !1;
    h.addChild(e, c, d, b, f);
    s_oStage.addChild(h);
  };
  this.unload = function () {
    h.off("mousedown", this._onExit);
    s_oStage.removeChild(h);
  };
  this._initListener = function () {
    h.on("mousedown", this._onExit);
  };
  this.show = function (a, e) {
    e
      ? ((b.text = TEXT_CONGRATS), (f.text = TEXT_CONGRATS))
      : ((b.text = TEXT_GAMEOVER), (f.text = TEXT_GAMEOVER));
    c.text = TEXT_SCORE + ": " + a;
    d.text = TEXT_SCORE + ": " + a;
    h.visible = !0;
    var k = this;
    createjs.Tween.get(h)
      .to({ alpha: 1 }, 500)
      .call(function () {
        k._initListener();
      });
    $(s_oMain).trigger("save_score", a);
    $(s_oMain).trigger("end_level", 1);
    $(s_oMain).trigger("share_event", a);
  };
  this._onExit = function () {
    h.off("mousedown");
    $(s_oMain).trigger("show_interlevel_ad");
    s_oGame.onExit();
  };
  this._init(a);
  return this;
}
function CCellGrid(a, e, c) {
  var d, f, b;
  this._init = function (a, c, e) {
    d = !1;
    f = "state_0";
    b = createSprite(e, "state_0", 0, 0, CELL_WIDTH, CELL_HEIGHT);
    b.stop();
    b.setTransform(a, c, GRID_SCALE, GRID_SCALE);
    s_oStage.addChild(b);
  };
  this.unload = function () {
    s_oStage.removeChild(b);
  };
  this.reset = function () {
    f = "state_0";
    b.gotoAndStop(f);
  };
  this.setChecked = function () {
    d = !0;
  };
  this.changeState = function (a) {
    b.gotoAndStop("state_" + a);
    f = "state_" + a;
  };
  this.putMine = function () {
    b.gotoAndStop("mine");
    f = "mine";
  };
  this.getState = function () {
    return f;
  };
  this.isChecked = function () {
    return d;
  };
  this._init(a, e, c);
}
