<!DOCTYPE html>
<html>
  <head>
    <title></title>
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x"
      crossorigin="anonymous"
    />
    <link rel="stylesheet" href="css/reset.css" type="text/css" />
    <link rel="stylesheet" href="css/main.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <meta
      name="viewport"
      content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, minimal-ui"
    />
    <meta name="msapplication-tap-highlight" content="no" />

    <script type="text/javascript" src="js/jquery-2.0.3.min.js"></script>
    <script type="text/javascript" src="js/createjs-2013.12.12.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
  </head>
  <body ondragstart="return false;" ondrop="return false;">
    <div
      style="
        position: fixed;
        background-color: transparent;
        top: 0px;
        left: 0px;
        width: 100%;
        height: 100%;
      "
    ></div>
    <script>

      var session_id, email_id, username;
      const apiUrl = "https://zo3gt1i8te.execute-api.ap-south-1.amazonaws.com/prod";

      $(document).ready(function () {
        var oMain = new CMain({
          rows: 20, //MIN 10 MAX 30
          cols: 20, //MIN 10 MAX 30
          mines: 70, //NUMBER OF MINES IN GRID
          time: 600000, //LEVEL TIME IN MILLISECONDS
        });

        axios.get(apiUrl + '/leaderboard/1').then(function (res) {
          displayLeaderboard(res.data.body);
        })
        .catch(function (error) {
          console.log(error);
        });

        $(oMain).on('start_session', function (evt) {
          if (parent.__ctlArcadeStartSession) {
            parent.__ctlArcadeStartSession();
          }
          //...ADD YOUR CODE HERE EVENTUALLY
          session_id = localStorage.getItem('userid');
          email_id = localStorage.getItem('email');
          username = localStorage.getItem('username');

        });

        $(oMain).on('end_session', function (evt) {
          if (parent.__ctlArcadeEndSession) {
            parent.__ctlArcadeEndSession();
          }
          //...ADD YOUR CODE HERE EVENTUALLY
        });

        $(oMain).on('save_score', function (evt, iScore) {
          if (parent.__ctlArcadeSaveScore) {
            parent.__ctlArcadeSaveScore({ score: iScore });
          }
          //...ADD YOUR CODE HERE EVENTUALLY
          const body = {'sessionId': session_id, 'username': username, 'email': email_id, 'gameId': 1, 'score': iScore};
          console.log('Saving score to db...');
          axios.post(apiUrl + "/save-score", body).then(function (res) {
            console.log(res);
          }).
          catch(function (error) {
            console.log(error);
          });
        });

        $(oMain).on('start_level', function (evt, iLevel) {
          if (parent.__ctlArcadeStartLevel) {
            parent.__ctlArcadeStartLevel({ level: iLevel });
          }
          //...ADD YOUR CODE HERE EVENTUALLY
        });

        $(oMain).on('end_level', function (evt, iLevel) {
          if (parent.__ctlArcadeEndLevel) {
            parent.__ctlArcadeEndLevel({ level: iLevel });
          }
          //...ADD YOUR CODE HERE EVENTUALLY
        });

        $(oMain).on('show_interlevel_ad', function (evt) {
          if (parent.__ctlArcadeShowInterlevelAD) {
            parent.__ctlArcadeShowInterlevelAD();
          }
          //...ADD YOUR CODE HERE EVENTUALLY
        });

        $(oMain).on('share_event', function (evt, iScore) {
          if (parent.__ctlArcadeShareEvent) {
            parent.__ctlArcadeShareEvent({
              img: '200x200.jpg',
              title: 'Congratulations!',
              msg:
                'You collected <strong>' +
                iScore +
                ' points</strong>!<br><br>Share your score with your friends!',
              msg_share:
                'My score is ' + iScore + ' points! Can you do better?',
            });
          }
          //...ADD YOUR CODE HERE EVENTUALLY
        });

        if (isIphone()) {
          setTimeout(function () {
            sizeHandler();
          }, 200);
        } else {
          sizeHandler();
        }

        const displayLeaderboard = (data) => {
          data = data.sort((a,b) => {
            if(a.score === b.score) {
              return b.updatedAt - a.updatedAt;
            }
            return b.score - a.score;
          })
          let html = '';
          data.forEach((ele, idx) => {
            html += `<tr>
                  <th>${idx + 1}</th>
                  <td>${ele.username}</td>
                  <td>${ele.score}</td>
                </tr>`
          })

          document.querySelector('#leaderboard tbody').innerHTML = html;
        }
      });
    </script>
    <canvas id="canvas" class="ani_hack" width="870" height="1504"> </canvas>
    <div
      id="block_game"
      style="
        position: fixed;
        background-color: transparent;
        top: 0px;
        left: 0px;
        width: 100%;
        height: 100%;
        display: none;
      "
    ></div>
    <div class="leaderboard">
      <div class="container-fluid g-0">
        <div class="row g-0">
          <div class="col-12 col-md-6 col-lg-4 mx-auto">
            <table class="table table-striped" id="leaderboard">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Username</th>
                  <th scope="col">Score</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
