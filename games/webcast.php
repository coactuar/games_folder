<?php
	require_once "config.php";
	
	if(!isset($_SESSION["user_email"]))
	{
		header("location: index.php");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            $logout_date   = date('Y/m/d H:i:s');
            $user_email=$_SESSION["user_email"];
            
            $query="UPDATE tbl_users set logout_date='$logout_date', logout_status='0' where user_email='$user_email'  and eventname='$event_name'";
            $res = mysqli_query($link, $query) or die(mysqli_error($link));

            unset($_SESSION["user_email"]);
            
            header("location: index.php");
            exit;
        }

    }
	
?>
<?php echo $_SESSION['users_id'];?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Sinamics Webcast</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<style>
.force-to-bottom {
    position:absolute;
    bottom: 0;
    
    width: 100%;
}
.bottom1{
    position:absolute;
    bottom: 17%;
    right: 10%;


}
body{
    background-image: url(img/2.jpg);
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
    overflow: hidden;
}
</style>
<body >
<!-- <div class="container-fluid">
 
    <div class="row ">
        <div class="col-12 text-right">

        <img src="img/logo.png" class="  logo"  style="width:180px; "  alt=""/> 
        </div> 
    </div>
    <div class="row ">
      <div class="col-12 col-md-10  ">
       
        </div>


        <div class="col-12 col-md-2 "> -->

<!-- <div class=" text-center">
<img src="img/Siemens.png" class="img-fluid logo" alt=""/> 

</div> -->
<div class="container overflow-hidden mt-5">
  <div class="row gx-5">
    <div class="col-md-6">
     <div class="p-3 border bg-light">
     <figure class="figure">
  <img src="img/bg_menu.jpg" class="figure-img img-fluid rounded " alt="...">
  <!-- <figcaption class="figure-caption text-end">A caption for the above image.</figcaption> -->
</figure>
<div class="card">
      <div class="card-body bg-warning">
        <h5 class="card-title">Battleship Minesweeper</h5>
        <!-- <p class="card-text">With supporting text below as a natural lead-in to additional content.</p> -->
        <a href="games/1/game/index.php" target="_blank" class="btn btn-primary">Play Game</a>
      </div>
    </div>
     </div>
    </div>
    <div class="col">
      <div class="p-3 border bg-light">
      <figure class="figure">
  <img src="img/bg_menu.jpg" class="figure-img img-fluid rounded " alt="...">
  <!-- <figcaption class="figure-caption text-end">A caption for the above image.</figcaption> -->
</figure>
<div class="card">
      <div class="card-body bg-warning">
        <h5 class="card-title">Battleship Minesweeper</h5>
        <!-- <p class="card-text">With supporting text below as a natural lead-in to additional content.</p> -->
        <a href="games/2/game/index.php" target="_blank" class="btn btn-primary">Play Game</a>
      </div>
    </div>
      </div>
    </div>
    
  </div>
</div>

<!-- <div class="container overflow-hidden mt-5">
  <div class="row gx-5">
    <div class="col">
     <div class="p-3 border bg-light">
     <figure class="figure">
  <img src="img/logo.png" class="figure-img img-fluid rounded" alt="...">
  <figcaption class="figure-caption text-end">A caption for the above image.</figcaption>
</figure>
<div class="card">
      <div class="card-body">
        <h5 class="card-title">Special title treatment</h5>
        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
        <a href="#" class="btn btn-primary">Go somewhere</a>
      </div>
    </div>
     </div>
    </div>
    <div class="col">
      <div class="p-3 border bg-light">
      <figure class="figure">
  <img src="img/logo.png" class="figure-img img-fluid rounded" alt="...">
  <figcaption class="figure-caption text-end">A caption for the above image.</figcaption>
</figure>
<div class="card">
      <div class="card-body">
        <h5 class="card-title">Special title treatment</h5>
        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
        <a href="#" class="btn btn-primary">Go somewhere</a>
      </div>
    </div>
      </div>
    </div>
    
  </div>
</div> -->

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
localStorage.setItem('email', '<?php echo $_SESSION['user_email'];?>');
localStorage.setItem('userid', '<?php echo $_SESSION['user_id'];?>');  
localStorage.setItem('username', '<?php echo $_SESSION['user_name'];?>');

</script>
<script>


        $("#form1").hide();
$("#formButton").click(function(){

        $("#form1").toggle();
    });
$(function(){
    //$('#videolinks').html('If you are unable to watch video, <a href="#" onClick="changeVideo()">click here</a>');
	$(document).on('submit', '#question-form form', function()
    {  
            $.post('submitques.php', $(this).serialize(), function(data)
            {
                if(data=="success")
                {
                  $('#ques-message').text('Your question is submitted successfully.');
                  $('#ques-message').removeClass('alert-danger').addClass('alert-success').fadeIn().delay(2000).fadeOut();
                  $('#question-form').find("textarea").val('');
                }
                else 
                {
                  $('#ques-message').text(data);
                  $('#ques-message').removeClass('alert-success').addClass('alert-danger').fadeIn().delay(5000).fadeOut();
                }
                
            });
        
      
      return false;
    });
});

function update()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'update'},
         type: 'post',
         success: function(output) {
         }
});
}
setInterval(function(){ update(); }, 30000);


function changeVideo()
{
    var wc = $('#webcast').attr("src");
    console.log(wc);
    if(wc == "video.php")
    {
        $('#webcast').attr("src","video_bkup.php");
    }
    else
    {
        $('#webcast').attr("src","video.php");
    }
}

</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-13');
</script>

</body>
</html>